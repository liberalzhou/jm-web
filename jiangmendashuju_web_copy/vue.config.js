'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || '造价信息管理系统与共享平台' // page title

const port = process.env.port || process.env.npm_config_port || 8081 // dev port

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: false, //关闭eslint方法
  // lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,  // 关闭生产环境下的SourceMap映射文件
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      '/zx-web': { // czhui
        // target: 'http://172.16.12.30:8070',          // 远瓴生产接口域名
        // target: 'http://172.16.12.31:8070',          // 财贸生产接口域名
        // target: 'http://127.0.0.1:8070',             // 本地域名
        // target: 'http://172.16.12.31:8070',          // 海迈接口域名
        // target: 'http://172.16.4.3:8070',            // 测试接口域名1.0
        // target: 'http://172.16.4.3:8090',            // 测试接口域名1.1

        //target: 'http://172.16.4.3:8060',            // 测试接口域名2.0
        //target: 'http://172.16.11.184:8071',            // 测试接口域名2.1
        target: 'http://172.16.10.10:8072',            // 江门测试接口域名2.1

        //target: 'http://172.16.4.3:8060',            // 测试接口域名2.0

        // target: 'http://172.16.4.2:8070',            // 开发接口域名
        // target: 'http://172.16.10.230:8060',         // 蔡杨潇本地
        // target: 'http://172.16.10.137:8070',         // 罗富强本地
        // target: 'http://172.16.101.101:8185',          // 外部跳转
       // target: 'http://119.91.204.152:8060',          // 业举本地
        // target: 'http://10.160.32.26:8070',         // 江门跳转
        // target: 'http://10.160.32.26:8070',          // 江门
        changeOrigin: true,     // 是否跨域
        pathRewrite: {
          "^/zx-web": ""
        }
      }
    },
    // before: require('./mock/mock-server.js')
  },
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack(config) {
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    config.plugins.delete('prefetch')

    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
              chunks: 'all',
              cacheGroups: {
                libs: {
                  name: 'chunk-libs',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 10,
                  chunks: 'initial'
                },
                elementUI: {
                  name: 'chunk-elementUI',
                  priority: 20,
                  test: /[\\/]node_modules[\\/]_?element-ui(.*)/
                },
                commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'),
                  minChunks: 3,
                  priority: 5,
                  reuseExistingChunk: true
                }
              }
            })
          config.optimization.runtimeChunk('single')
        }
      )
  }
}
