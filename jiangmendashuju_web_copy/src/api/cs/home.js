import request from '@/utils/request'

// 获取首页数据
export function getHomePage(params) {
  return request({
    url: '/mpcsys/sett/getHomePage',
    method: 'get',
    params
  })
}

// 获取全国城市信息价数据
export function getNationwideCityPrice() {
  return request({
    url: '/mpcsys/sett/getNationwideCityPrice',
    method: 'get',
  })
}