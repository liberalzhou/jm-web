import request from '@/utils/request'

// 查询全部供应库列表
export function getAllSupplierList(params) {
    return request({
        url: '/supplier/getAllSupplierList',
        method: 'post',
        params
    })
}

// 查询供应商库分页列表
export function getSupplierPageList(data) {
    return request({
        url: '/supplier/getSupplierPageList',
        method: 'post',
        data
    })
}

// 新增或更新供应商信息
export function addOrUpdateSupplier(data) {
    return request({
        url: '/supplier/addOrUpdateBrand',
        method: 'post',
        data
    })
}

// 删除供应商信息
export function deleteSupplier(params) {
    return request({
        url: '/supplier/deleteSupplier',
        method: 'post',
        params
    })
}

// 查询供应商条件信息(国标分类和自定义分类和城市)
export function getSupplierCondition(params) {
    return request({
        url: '/supplier/getSupplierCondition',
        method: 'post',
        params
    })
}

// 导出供应商列表
export function exportSupplier(data) {
    return request({
        url: '/supplier/exportSupplier',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

// 查询供应商库详细信息
export function getSupplierInfo(params) {
    return request({
        url: '/supplier/getSupplierInfo',
        method: 'post',
        params
    })
}

// 查询全部品牌列表
export function getAllBrandList(params) {
    return request({
        url: '/brand/getAllBrandList',
        method: 'post',
        params
    })
}

// 查询品牌库分页列表
export function getBrandPageList(data) {
    return request({
        url: '/brand/getBrandPageList',
        method: 'post',
        data
    })
}

// 新增或更新品牌信息
export function addOrUpdateBrand(data) {
    return request({
        url: '/brand/addOrUpdateBrand',
        method: 'post',
        data
    })
}

// 删除品牌
export function deleteBrand(params) {
    return request({
        url: '/brand/deleteBrand',
        method: 'post',
        params
    })
}

// 导出品牌列表
export function exportBrand(data) {
    return request({
        url: '/brand/exportBrand',
        method: 'post',
        data,
        responseType: 'blob'
    })
}