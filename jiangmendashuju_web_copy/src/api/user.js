import request from '@/utils/request'

// 获取图形验证码
export function authImg() {
  return request({
    url: '/sys/login/authImg',
    method: 'get',
    responseType: 'arraybuffer'
  })
}

// 登录
export function login(params) {
  return request({
    url: '/sys/login/login',
    method: 'post',
    params
  })
}

// 退出
export function logout(params) {
  return request({
    url: '/sys/login/logout',
    method: 'post',
    params
  })
}

// 获取当前用户信息
export function getCurUser() {
  return request({
    url: '/sys/user/getCurUser',
    method: 'post'
  })
}

// 获取系统信息
export function getSysInfo() {
  return request({
    url: '/sys/getSysInfo',
    method: 'post'
  })
}

// 配置系统名称
export function configSysName(params) {
  return request({
    url: '/sys/configSysName',
    method: 'post',
    params
  })
}

// 获取账号列表
export function getUserPageList(data) {
  return request({
    url: '/sys/getUserPageList',
    method: 'post',
    data
  })
}

// 新增或更新账号
export function addOrUpdateUser(data) {
  return request({
    url: '/sys/addOrUpdateUser',
    method: 'post',
    data
  })
}

// 重置账号密码
export function resetPwd(params) {
  return request({
    url: '/sys/resetPwd',
    method: 'post',
    params
  })
}

// 更换角色
export function updateRole(params) {
  return request({
    url: '/sys/updateRole',
    method: 'post',
    params
  })
}

// 删除账号
export function delUser(params) {
  return request({
    url: '/sys/delUser',
    method: 'post',
    params
  })
}

// 获取角色列表
export function getRoleList(data) {
  return request({
    url: '/sys/getRoleList',
    method: 'post',
    data
  })
}

// 删除角色
export function delRole(params) {
  return request({
    url: '/sys/delRole',
    method: 'post',
    params
  })
}

// 修改账号信息（个人信息）
export function updateInfo(data) {
  return request({
    url: '/sys/user/updateInfo',
    method: 'post',
    data
  })
}

// 修改密码
export function updatePwd(params) {
  return request({
    url: '/sys/user/updatePwd',
    method: 'post',
    params
  })
}

// 上传用户头像
export function uploadUserImg(data) {
  return request({
    url: '/sys/user/uploadUserImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 查询菜单树状列表
export function getMenuTree() {
  return request({
    url: '/sys/getMenuTree',
    method: 'post'
  })
}

// 新增或更新角色
export function addOrUpdateRole(data) {
  return request({
    url: '/sys/addOrUpdateRole',
    method: 'post',
    data
  })
}

// 启用/禁用角色接口
export function updateRoleStatus(data) {
  return request({
    url: '/sys/updateRoleStatus',
    method: 'post',
    data
  })
}

// 获取角色详细信息
export function getRoleInfo(params) {
  return request({
    url: '/sys/getRoleInfo',
    method: 'post',
    params
  })
}

// 采算新增---------start

// 批量开通采集员账号
export function batchUsers(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/batchAddAccount',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 采集员审核状态修改
export function changeAuditState(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/auditCollectMessagerInfo',
    method: 'post',
    params
  })
}

// 采集员上传材价
export function collectrImportMatPrice(data) {
  return request({
    url: '/collect/messager/uploadDetail/uploadTempFile',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 创建或更新采集员信息
export function createOrUpdateUser(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/addOrUpdateCollectMessager',
    method: 'post',
    data
  })
}

// 删除采集员信息
export function deleteUsers(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/deleteCollectMessagerInfo',
    method: 'post',
    params
  })
}

// 采集员材价文件模板下载
export function downloadTempFile(params) {
  return request({
    url: '/collect/messager/uploadDetail/downloadTempFile',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 导出采集员信息
export function exportUsers(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/exportCollectMessagerInfoFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 下载批量开通采集员账号模板
export function getAddAccountImportTemplate() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getAddAccountImportTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 查询审核标准信息
export function getAuditInfo() {
  return request({
    url: '/mpccollect/config/getAuditInfo',
    method: 'post'
  })
}

// 查询邮箱列表
export function getEmailList(data) {
  return request({
    url: '/mpccollect/config/getEmailList',
    method: 'post'
  })
}

// 查询采集员通知列表
export function getNoticeListPage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/getNoticeListPage',
    method: 'post',
    data
  })
}

// 获取地区
export function getRegion() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getAddressTree',
    method: 'post'
  })
}

// 查询通知模板列表
export function getTemplateInfoList(data) {
  return request({
    url: '/mpccollect/config/getTemplateInfoList',
    method: 'post',
    data
  })
}

// 获取采集员上传材价模板列表
export function getUploadMaterialPriceList(data) {
  return request({
    url: '/collect/messager/uploadDetail/selectUploadDetail',
    method: 'post',
    data
  })
}

// 重置密码
export function initPassword(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/resetPwd',
    method: 'post',
    params
  })
}

// 查询采集员列表
export function getUsersList(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/getCollectMessagerInfoDetailPage',
    method: 'post',
    data
  })
}

// 发送邮箱
export function sendEmailMessage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/sendEmailMessage',
    method: 'post',
    data
  })
}

// 发送短信
export function sendSmsMessage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/sendSmsMessage',
    method: 'post',
    data
  })
}
// 采算新增----------end
