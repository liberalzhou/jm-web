import request from '@/utils/request'

// 获取(材价)类型列表
export function getDictList(params) {
  return request({
    url: '/sys/common/getDictList',
    method: 'post',
    params
  })
}

// 获取(材价)自定义分类
export function getCustomClassify(params) {
  return request({
    url: '/sys/common/getCustomClassify',
    method: 'post',
    params
  })
}

// 下载失败清单文件
export function downloadErrorExcel(params) {
  return request({
    url: '/sys/common/downloadErrorExcel',
    method: 'get',
    responseType: 'blob',
    params
  })
}

// 获取地区树状列表
export function getAreaTree() {
  return request({
    url: '/sys/common/getAreaTree',
    method: 'post'
  })
}

// 获取国标分类树状数据
export function getSubClassifyTree() {
  return request({
    url: '/sys/common/getSubClassifyTree',
    method: 'post'
  })
}

// 新增类型（材价来源/材价类型）
export function addDict(params) {
  return request({
    url: '/sys/common/addDict',
    method: 'post',
    params
  })
}

// 删除类型（材价来源/材价类型）
export function delDict(params) {
  return request({
    url: '/sys/common/delDict',
    method: 'post',
    params
  })
}

// 修改类型（材价来源/材价类型）
export function updateDict(params) {
  return request({
    url: '/sys/common/updateDict',
    method: 'post',
    params
  })
}

// 获取一级国标分类列表
export function getSuperiorSubcList() {
  return request({
    url: '/sys/common/getSuperiorSubcList',
    method: 'post',
  })
}

// 新增或更新自定义分类
export function addOrUpdateCustomClassify(data) {
  return request({
    url: '/sys/common/addOrUpdateCustomClassify',
    method: 'post',
    data
  })
}

// 删除自定义分类
export function delCustomClassify(params) {
  return request({
    url: '/sys/common/delCustomClassify',
    method: 'post',
    params
  })
}

// 自定义分类绑定国标
export function typeAssociate(params) {
  return request({
    url: '/sys/common/typeAssociate',
    method: 'post',
    params
  })
}

// 公有云地址
export function publicUrl() {// czhui
  // return 'http://172.16.4.3:8003' // 测试
  return 'http://172.16.12.30:8003' // 生产
  // return 'http://172.16.12.31:8003' // 生产
}
