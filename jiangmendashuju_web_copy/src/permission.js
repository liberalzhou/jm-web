import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import getPageTitle from '@/utils/get-page-title'

import { clearStorage } from '@/utils/clearStorage'
import { getCurUser, getSysInfo, } from '@/api/user'

NProgress.configure({ showSpinner: false }) // 进度条

const whiteList = ['/login','/project/importProject', '/materialManage/index'] // 不重定向白名单

router.beforeEach(async(to, from, next) => {
  NProgress.start()

  document.title = getPageTitle(to.meta.title)

  if (store.getters.token) { // 是否登录(改架构,由请求中Cookie改为token鉴权)
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      if (store.getters.userId) { // 是否已有用户信息
        next()
      } else {
        getCurUserApi(to,next)
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) { // 白名单
      let token = to.query.token

      if (!token) {
        let str = window.location.search.substring(1);
        let arr = str.split("token=");
        token = arr[1];
      }
      
      if(token) {// 针对海迈跳转免登录
        store.commit('user/SET_TOKEN', token)
        sessionStorage.setItem('token', token)
        getSysInfoApi()
        getCurUserApi(to,next)
      } else {
        next()
      }
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

// 获取用户信息
function getCurUserApi(to,next) {
  getCurUser().then((res) => {
    sessionStorage.setItem('user_account', res.body.userAccount)
    sessionStorage.setItem('user_name', res.body.userName)
    // sessionStorage.setItem('id', res.body.userId) // 去掉缓存，刷新页面可重置路由表
    sessionStorage.setItem('phone', res.body.phone)
    sessionStorage.setItem('user_head', res.body.avatarUrl)
    sessionStorage.setItem('user_department', res.body.department)
    sessionStorage.setItem('user_position', res.body.position)
    sessionStorage.setItem('user_qq', res.body.qq)
    store.commit('user/SET_USER_ACCOUNT', res.body.userAccount)
    store.commit('user/SET_NAME', res.body.userName)
    store.commit('user/SET_ID', res.body.userId)
    store.commit('user/SET_PHONE', res.body.phone)
    store.commit('user/SET_USER_HEAD', res.body.avatarUrl)
    store.commit('user/SET_USER_DEPARTMENT', res.body.department)
    store.commit('user/SET_USER_POSITION', res.body.position)
    store.commit('user/SET_USER_QQ', res.body.qq)

    // 拥有的权限列表
    var permissionVOs = []
    // 生成功能权限表
    var btnRoles = []
    // 生成动态路由表
    var roles = []

    // menuType：1菜单 2功能
    try {
      permissionVOs = res.body.userMenuList
      for (var i = 0; i < permissionVOs.length; i++) {
        if (permissionVOs[i].menuType == '1') {
          roles.push(permissionVOs[i].menuKey)
        } else if (permissionVOs[i].menuType == '2') {
          btnRoles.push(permissionVOs[i].menuKey)
        }
      }
    } catch(err) {
      console.log(err)
    }
    store.commit('user/SET_BTN_ROLES', btnRoles)
    // 生成路由表
    try {
      store.dispatch('permission/generateRoutes', roles).then(res => {
        router.addRoutes(res)
        let query = to.query
        if(query.token) {
          next({path: to.path, replace: true, query: {
            types: 4,
            id: query.id
          } })
        } else {
          next({ ...to, replace: true })
        }
      })
    } catch(err) {
      console.log(err)
    }
  }).catch(err => {
    clearStorage()
    next('/login')
    NProgress.done()
  })
}

// 查询系统信息
function getSysInfoApi() {
  getSysInfo().then(res => {
    if (res.code == '01') {
      try{
        sessionStorage.setItem('belong_company', res.body.belongCompany)
        store.commit('user/SET_BELONG_COMPANY', res.body.belongCompany)
        sessionStorage.setItem('sys_name', res.body.sysName)
        store.commit('user/SET_SYS_NAME', res.body.sysName)
        sessionStorage.setItem('sys_uuid', res.body.sysUuid)
        store.commit('user/SET_SYS_UUID', res.body.sysUuid)
        sessionStorage.setItem('version', res.body.version)
        store.commit('user/SET_VERSION', res.body.version)
        sessionStorage.setItem('technical_support', res.body.technicalSupport)
        store.commit('user/SET_TECHNICAL_SUPPORT', res.body.technicalSupport)
        sessionStorage.setItem('update_time', res.body.updateTime)
        store.commit('user/SET_UPDATE_TIME', res.body.updateTime)
      }catch(err){
        console.log(err)
      }
    }
  })
}

router.afterEach(() => {
  NProgress.done()
})
