import Layout from '@/layout'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'iconfont icon-sy' },
      hidden: true // czhui
    }]
  }

  // czhui test
  // {
  //   path: '/test',
  //   name: 'Tset',
  //   component: () => import('@/views/test'),
  //   meta: { title: '测试'},
  //   hidden: true
  // },
]

export const asyncRoutes = [
  {
    path: '/',
    redirect: '/cs/dashboard',
    component: Layout,
    children: [{
      path: 'cs/dashboard',
      name: 'cs/Dashboard',
      meta: { title: '首页', icon: 'iconfont iconshouye_icon' },
      component: () => import('@/views/cs/dashboard/index')
    }]
  },
  {
    path: '/synthesizeManager',
    component: Layout,
    redirect: '/cs/synthesizeManager/index',
    meta: { title: '材价管理', icon: 'iconfont iconzonghejia_icon' },
    alwaysShow: true,
    children: [{
      path: 'index',
      name: 'ModeList',
      meta: { title: '模板库' },
      component: () => import('@/views/cs/synthesizeManager/index')
    }, {
      path: 'modeDetail',
      name: 'ModeDetail',
      redirect: '/synthesizeManager/index',
      meta: { title: '模板库' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/modeDetail'),
      children: [{
        path: 'index',
        name: 'ModeDetailIndex',
        meta: { title: '模板库详情' },
        hidden: true
      }]
    },
    {
      path: 'synthesizeStock',
      name: 'SynthesizeStock',
      meta: { title: '综合价库' },
      component: () => import('@/views/cs/synthesizeManager/synthesizeStock')
    },
    {
      path: 'synthesizeAudit',
      name: 'SynthesizeAudit',
      meta: { title: '综合价评审' },
      component: () => import('@/views/cs/synthesizeManager/synthesizeAudit')
    }, {
      path: 'synthesizePriceDetail',
      name: 'SynthesizePriceDetail',
      redirect: '/synthesizeManager/synthesizeAudit',
      meta: { title: '综合价评审' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/synthesizePriceDetail'),
      children: [{
        path: 'index',
        name: 'SynthesizePriceDetailIndex',
        meta: { title: '评审详情' },
        hidden: true
      }]
    },
    {
      path: 'infoPriceStock',
      name: 'InfoPriceStock',
      meta: { title: '信息价管理' },
      component: () => import('@/views/cs/synthesizeManager/infoPriceStock')
    }, {
      path: 'infoPriceDetail',
      name: 'InfoPriceDetail',
      redirect: '/synthesizeManager/infoPriceStock',
      meta: { title: '信息价管理' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/infoPriceDetail'),
      children: [{
        path: 'index',
        name: 'InfoPriceDetailIndex',
        meta: { title: '信息价详情' },
        hidden: true
      }]
    }, {
      path: 'historyPriceDetail',
      name: 'HistoryPriceDetail',
      redirect: '/synthesizeManager/infoPriceStock',
      meta: { title: '信息价管理' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/historyPriceDetail'),
      children: [{
        path: 'index',
        name: 'HistoryPriceDetailIndex',
        meta: { title: '历史库详情' },
        hidden: true
      }]
    }, {
      path: 'recycleBinDetail',
      name: 'RecycleBinDetail',
      redirect: '/synthesizeManager/infoPriceStock',
      meta: { title: '信息价管理' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/recycleBinDetail'),
      children: [{
        path: 'index',
        name: 'RecycleBinDetailIndex',
        meta: { title: '回收站信息价详情' },
        hidden: true
      }]
    }, {
      path: 'periodManager',
      name: 'PeriodManager',
      meta: { title: '期刊管理' },
      component: () => import('@/views/cs/synthesizeManager/periodManager')
    }, {
      path: 'periodicalDetail',
      name: 'PeriodicalDetail',
      redirect: '/synthesizeManager/periodManager',
      meta: { title: '期刊管理' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/periodicalDetail'),
      children: [{
        path: 'index',
        name: 'PeriodicalDetailIndex',
        meta: { title: '期刊详情' },
        hidden: true
      }]
    }, {
      path: 'issueRecordDetail',
      name: 'IssueRecordDetail',
      redirect: '/synthesizeManager/periodManager',
      meta: { title: '期刊管理' },
      hidden: true,
      component: () => import('@/views/cs/synthesizeManager/issueRecordDetail'),
      children: [{
        path: 'index',
        name: 'IssueRecordDetailIndex',
        meta: { title: '发布记录信息价详情' },
        hidden: true
      }]
    }]
  },
  {
    path: '/userManage',
    component: Layout,
    redirect: '/userManage/index',
    alwaysShow: true,
    meta: { title: '采集员管理', icon: 'iconfont iconrenyuanguanli' },
    children: [
      {
        path: 'index',
        name: 'UserManage',
        meta: { title: '人员管理' },
        component: () => import('@/views/cs/materialManage/userManage')
      }
    ]
  },
  {
    path: '/cs/setting',
    component: Layout,
    redirect: '/cs/setting/modeSet',
    meta: { title: '设置中心', icon: 'iconfont iconshezhi_icon' },
    children: [{
      path: 'homeSet',
      name: 'HomeSet',
      meta: { title: '首页设置' },
      component: () => import('@/views/cs/setting/homeSet')
    }, {
      path: 'modeSet',
      name: 'ModeSet',
      meta: { title: '模板库设置' },
      component: () => import('@/views/cs/setting/modeSet')
    }, {
      path: 'synthesizeSet',
      name: 'SynthesizeSet',
      meta: { title: '综合价设置' },
      component: () => import('@/views/cs/setting/synthesizeSet')
    }, {
      path: 'infoSet',
      name: 'InfoSet',
      meta: { title: '信息价设置' },
      component: () => import('@/views/cs/setting/infoSet')
    }, {
      path: 'periodicalSet',
      name: 'PeriodicalSet',
      meta: { title: '期刊设置' },
      component: () => import('@/views/cs/setting/periodicalSet')
    }, {
      path: 'materialSet',
      name: 'MaterialSet',
      meta: { title: '材价库设置' },
      component: () => import('@/views/cs/setting/materialSet')
    }, {
      path: 'userSet',
      name: 'UserSet',
      meta: { title: '采集员设置' },
      component: () => import('@/views/cs/setting/userSet')
    }, { // 其他设置还没配权限
      path: 'otherSet',
      name: 'otherSet',
      meta: { title: '其他设置' },
      component: () => import('@/views/cs/setting/otherSet')
    }]
  },
  {
    path: '/inquiryManage',
    component: Layout,
    redirect: '/inquiryManage/index',
    meta: { title: '询价管理', icon: 'iconfont icon-xunjiaguanli' },
    children: [
      {
        path: 'index',
        name: 'Inquiry',
        component: () => import('@/views/inquiryManage/index'),
        meta: { title: '询价库' }
      },
      {
        path: 'toInquiry',
        name: 'ToInquiry',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { title: '询价', activeMenu: '/inquiryManage/index' },
        hidden: true
      },
      { // 为了让侧边栏选中询价审核
        path: 'editInquiry',
        name: 'EditInquiry',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { title: '编辑询价', activeMenu: '/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'inquiryDetails',
        name: 'InquiryDetails',
        component: () => import('@/views/inquiryManage/inquiryDetails'),
        meta: { title: '项目批次详情', activeMenu: '/inquiryManage/index' },
        hidden: true
      },
      {
        path: 'inquiryExamine',
        name: 'InquiryExamine',
        component: () => import('@/views/inquiryManage/inquiryExamine'),
        meta: { title: '询价审核' }
      },
      {
        path: 'examinePass',
        name: 'ExaminePass',
        component: () => import('@/views/inquiryManage/examinePass'),
        meta: { title: '审核通过数据', activeMenu: '/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'examineFail',
        name: 'ExamineFail',
        component: () => import('@/views/inquiryManage/examineFail'),
        meta: { title: '审核不通过数据', activeMenu: '/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'examinePend',
        name: 'ExaminePend',
        component: () => import('@/views/inquiryManage/examinePend'),
        meta: { title: '待审核数据', activeMenu: '/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'inquiryDrafts',
        name: 'InquiryDrafts',
        component: () => import('@/views/inquiryManage/inquiryDrafts'),
        meta: { title: '草稿箱' }
      },
      { // 为了让侧边栏选中草稿箱
        path: 'toInquiryDrafts',
        name: 'ToInquiryDrafts',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { title: '询价', activeMenu: '/inquiryManage/inquiryDrafts' },
        hidden: true
      }
    ]
  },

  {
    path: '/materialManage',
    component: Layout,
    redirect: '/materialManage/index',
    alwaysShow: true,
    meta: { title: '材价管理', icon: 'iconfont icon-caijiaguanli12' },
    children: [
      {
        path: 'index',
        name: 'Material',
        component: () => import('@/views/materialManage/index'),
        meta: { title: '材价库' }
      },
      {
        path: 'checkMaterial',
        name: 'CheckMaterial',
        component: () => import('@/views/materialManage/checkMaterial'),
        meta: { title: '造价通查价' }
      },
      {
        path: 'sameMaterial',
        name: 'SameMaterial',
        component: () => import('@/views/materialManage/sameMaterial'),
        meta: { title: '相同材价', activeMenu: '/materialManage/index' },
        hidden: true
      },
      {
        path: 'checkResult',
        name: 'CheckResult',
        component: () => import('@/views/materialManage/detail/checkResult'),
        meta: { title: '查价结果', activeMenu: '/materialManage/checkMaterial' },
        hidden: true
      },
      {
        path: 'checkHistory',
        name: 'CheckHistory',
        component: () => import('@/views/materialManage/detail/checkHistory'),
        meta: { title: '查价历史', activeMenu: '/materialManage/checkMaterial' },
        hidden: true
      }
    ]
  },

  {
    path: '/shareManage',
    component: Layout,
    redirect: '/shareManage/index',
    meta: { title: '分享管理', icon: 'iconfont icon-fenxiangguanli' },
    // hidden: true, // czhui
    children: [
      {
        path: 'index',
        name: 'Share',
        component: () => import('@/views/shareManage/index'),
        meta: { title: '分享库' }
      },
      {
        path: 'shareExamine',
        name: 'ShareExamine',
        component: () => import('@/views/shareManage/shareExamine'),
        meta: { title: '分享审核' }
      },
      {
        path: 'shareDetail',
        name: 'ShareDetail',
        component: () => import('@/views/shareManage/shareDetail'),
        meta: { title: '分享详情', activeMenu: '/shareManage/index' },
        hidden: true
      },
      {
        path: 'shareEdit',
        name: 'ShareEdit',
        component: () => import('@/views/shareManage/shareEdit'),
        meta: { title: '编辑分享', activeMenu: '/shareManage/index' },
        hidden: true
      },
      {
        path: 'shareAdd',
        name: 'ShareAdd',
        component: () => import('@/views/shareManage/shareAdd'),
        meta: { title: '添加分享', activeMenu: '/shareManage/index' },
        hidden: true
      },
      {
        path: 'shareExamineDetail',
        name: 'ShareExamineDetail',
        component: () => import('@/views/shareManage/shareExamineDetail'),
        meta: { title: '分享审核详情', activeMenu: '/shareManage/shareExamine' },
        hidden: true
      }
    ]
  },

  {
    path: '/project',
    component: Layout,
    redirect: '/project/index',
    meta: { title: '项目管理', icon: 'iconfont icon-a-22' },
    children: [
      {
        path: 'detail',
        name: 'Detail',
        component: () => import('@/views/project/detail'),
        meta: { title: '项目详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'index',
        name: 'Projects',
        component: () => import('@/views/project/index'),
        meta: { title: '项目库' }
      },
      {
        path: 'importProject',
        name: 'ImportProject',
        component: () => import('@/views/project/importProject'),
        meta: { title: '导入项目' }
      },
      {
        path: 'compareProject',
        name: 'CompareProject',
        // component: () => import('@/views/project/compareProject'),
        component: () => import('@/views/project/projectComparison'),
        meta: { title: '项目对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'compareStage',
        name: 'compareStage',
        // component: () => import('@/views/project/compareStage'),
        component: () => import('@/views/project/wholeComparison'),
        meta: { title: '阶段对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'projectVisible',
        name: 'ProjectVisible',
        component: () => import('@/views/project/projectVisible'),
        meta: { title: '可视化', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'report',
        name: 'Peport',
        component: () => import('@/views/project/testing/index'),
        meta: { title: '体检报告', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'testing',
        name: 'Testing',
        component: () => import('@/views/project/testing/doing'),
        meta: { title: '项目体检', activeMenu: '/project/index' },
        hidden: true
      }
    ]
  },

  {
    path: '/target',
    component: Layout,
    redirect: '/target/index',
    meta: { title: '指标管理', icon: 'iconfont icon-zhibiaoku11' },
    children: [
      {
        path: 'index',
        name: 'Targets',
        component: () => import('@/views/target/index'),
        meta: { title: '指标库' }
      },
      {
        path: 'synthesize',
        name: 'synthesize',
        component: () => import('@/views/target/synthesizeIndex'),
        meta: { title: '综合指标库' }
      },
      {
        path: 'synthesizeExamine',
        name: 'SynthesizeExamine',
        component: () => import('@/views/target/synthesizeExamine'),
        meta: { title: '综合指标审核' }
      },
      {
        path: 'detail',
        name: 'TargetDetail',
        component: () => import('@/views/target/detail/index'),
        meta: { title: '指标库详情', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'calculate',
        name: 'Calculate',
        component: () => import('@/views/target/indexCalculation'),
        meta: { title: '指标计算', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'analysis',
        name: 'Analysis',
        component: () => import('@/views/target/indexAnalysis'),
        meta: { title: '指标分析', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'synthesizeDetail',
        name: 'SynthesizeDetail',
        component: () => import('@/views/target/detail/synthesizeCalculate'),
        meta: { title: '综合指标库详情', activeMenu: '/target/synthesize' },
        hidden: true
      }
    ]
  },

  {
    path: '/targetApplication',
    component: Layout,
    redirect: '/targetApplication/index',
    meta: { title: '新项目智能应用', icon: 'iconfont icon-zhibiaoyingyong' },
    children: [
      {
        path: 'index',
        name: 'TargetCost',
        component: () => import('@/views/targetApplication/index'),
        meta: { title: '快速造价' }
      },
      {
        path: 'newCalcula',
        name: 'NewCalcula',
        component: () => import('@/views/targetApplication/newCalculation'),
        meta: { title: '快速造价', activeMenu: '/targetApplication/index' },
        hidden: true
      },
      {
        path: 'calculaDetail',
        name: 'CalculaDetail',
        component: () => import('@/views/targetApplication/calculaDetail'),
        meta: { title: '测算方案详情', activeMenu: '/targetApplication/index' },
        hidden: true
      },
      {
        path: 'targetReal',
        name: 'TargetReal',
        component: () => import('@/views/targetApplication/targetRealtime'),
        meta: { title: '实时指标' }
      },
      {
        path: 'targetTest',
        name: 'TargetTest',
        component: () => import('@/views/targetApplication/targetTesting'),
        meta: { title: '智能检测' }
      }
    ]
  },

  {
    path: '/supplier',
    component: Layout,
    redirect: '/supplier/index',
    meta: { title: '供应商管理', icon: 'iconfont icon-zhibiaoku11' },
    children: [
      {
        path: 'index',
        name: 'Supplier',
        component: () => import('@/views/supplier/index'),
        meta: { title: '供应商库' }
      },
      {
        path: 'brandIndex',
        name: 'BrandIndex',
        component: () => import('@/views/supplier/brandIndex'),
        meta: { title: '品牌库' }
      },
      {
        path: 'addSupplier',
        name: 'AddSupplier',
        component: () => import('@/views/supplier/detail/addSupplier'),
        meta: { title: '添加供应商', activeMenu: '/supplier/index' },
        hidden: true
      },
      {
        path: 'detail',
        name: 'supplierDetail',
        component: () => import('@/views/supplier/detail/supplierDetail'),
        meta: { title: '供应商详情', activeMenu: '/supplier/index' },
        hidden: true
      },
      {
        path: 'brandDetail',
        name: 'BrandDetail',
        component: () => import('@/views/supplier/detail/brandDetail'),
        meta: { title: '品牌库详情', activeMenu: '/supplier/brandIndex' },
        hidden: true
      }
    ]
  },

  {
    path: '/file',
    component: Layout,
    redirect: '/file/index',
    meta: { title: '文档管理', icon: 'iconfont icon-wendangku1' },
    children: [
      {
        path: 'index',
        name: 'File',
        component: () => import('@/views/file/index'),
        meta: { title: '材价文档' }
      },
      {
        path: 'indexDetail',
        name: 'IndexDetail',
        component: () => import('@/views/file/detail/indexDetail'),
        meta: { title: '材价文档详情', activeMenu: '/file/index' },
        hidden: true
      },
      {
        path: 'projectIndex',
        name: 'ProjectIndex',
        component: () => import('@/views/file/projectIndex'),
        meta: { title: '项目文档' }
      },
      {
        path: 'projectDetail',
        name: 'ProjectDetail',
        component: () => import('@/views/file/detail/projectDetail'),
        meta: { title: '项目文档详情', activeMenu: '/file/projectDetail' },
        hidden: true
      },
      {
        path: 'projectItemDetail',
        name: 'ProjectItemDetail',
        component: () => import('@/views/file/detail/projectItemDetail'),
        meta: { title: '造价文件详情', activeMenu: '/file/projectItemDetail' },
        hidden: true
      },
      {
        path: 'businessIndex',
        name: 'BusinessIndex',
        component: () => import('@/views/file/businessIndex'),
        meta: { title: '企业文档' }
      },
      {
        path: 'businessDetail',
        name: 'BusinessDetail',
        component: () => import('@/views/file/detail/businessDetail'),
        meta: { title: '企业文档详情', activeMenu: '/file/businessDetail' },
        hidden: true
      }
    ]
  },

  {
    path: '/projectSet',
    component: Layout,
    redirect: '/projectSet/costStage',
    alwaysShow: true,
    // hidden: true,
    meta: { title: '项目库设置', icon: 'iconfont icon-zbk1' },
    children: [
      {
        path: 'costStage',
        name: 'CostStage',
        component: () => import('@/views/projectSet/costStage'),
        meta: { title: '造价阶段标准配置' }
      },
      {
        path: 'unitProject',
        name: 'UnitProject',
        component: () => import('@/views/projectSet/unitProject'),
        meta: { title: '单位工程标准表配置' }
      },
      {
        path: 'branchProject',
        name: 'BranchProject',
        component: () => import('@/views/projectSet/branchProject'),
        meta: { title: '分部工程标准表配置', activeMenu: '/projectSet/unitProject' },
        hidden: true
      },
      {
        path: 'skillFeature',
        name: 'SkillFeature',
        component: () => import('@/views/projectSet/skillFeature'),
        meta: { title: '技术特征配置' }
      },
      {
        path: 'quotaStandard',
        name: 'QuotaStandard',
        component: () => import('@/views/projectSet/quotaStandard'),
        meta: { title: '造价定额规范配置' }
      }
    ]
  },
  {
    path: '/targetSet',
    component: Layout,
    redirect: '/targetSet/earlyWarn',
    alwaysShow: true,
    // hidden: true,
    meta: { title: '指标设置', icon: 'iconfont icon-zhibiaokushezhi' },
    children: [
      {
        path: 'earlyWarn',
        name: 'EarlyWarn',
        component: () => import('@/views/targetSet/earlyWarning'),
        meta: { title: '预警设置' }
      },
      {
        path: 'addWarn',
        name: 'AddWarn',
        component: () => import('@/views/targetSet/operates/addEarlyWarn'),
        meta: { title: '配置预警指标', activeMenu: '/targetSet/earlyWarn' },
        hidden: true
      },
      {
        path: 'economicTarget',
        name: 'EconomicTarget',
        component: () => import('@/views/targetSet/economicTarget'),
        meta: { title: '常用综合经济指标配置' }
      },
      {
        path: 'addEconomic',
        name: 'AddEconomic',
        component: () => import('@/views/targetSet/operates/addEconomicTarget'),
        meta: { title: '配置常用综合经济指标', activeMenu: '/targetSet/economicTarget' },
        hidden: true
      },
      {
        path: 'technologyTarget',
        name: 'TechnologyTarget',
        component: () => import('@/views/targetSet/technologyTarget'),
        meta: { title: '常用综合技术指标配置' }
      },
      {
        path: 'addTechnology',
        name: 'AddTechnology',
        component: () => import('@/views/targetSet/operates/addTechnologyTarget'),
        meta: { title: '配置常用综合技术指标', activeMenu: '/targetSet/technologyTarget' },
        hidden: true
      }
      // {
      //   path: 'economicConfigure',
      //   name: 'EconomicConfigure',
      //   component: () => import('@/views/targetSet/economicConfigure'),
      //   meta: { title: '经济指标生成配置' }
      // },
      // {
      //   path: 'technologyConfigure',
      //   name: 'TechnologyConfigure',
      //   component: () => import('@/views/targetSet/technologyConfigure'),
      //   meta: { title: '技术指标生成配置' }
      // },
    ]
  },

  {
    path: '/personal',
    component: Layout,
    redirect: '/personal/index',
    meta: { title: '个人中心', icon: 'iconfont icon-ger1' },
    hidden: true,
    children: [
      {
        path: 'index',
        name: 'UserInfo',
        component: () => import('@/views/personal/index'),
        meta: { title: '个人设置' }
      }
    ]
  },
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/index',
    meta: { title: '设置中心', icon: 'iconfont icon-shezhi' },
    children: [
      {
        path: 'index',
        name: 'System',
        component: () => import('@/views/setting/index'),
        meta: { title: '系统信息' }
      },
      {
        path: 'buyCombo',
        name: 'BuyCombo',
        redirect: '/setting/index',
        meta: { title: '系统信息' },
        hidden: true,
        component: () => import('@/views/setting/buyCombo'),
        children: [
          {
            path: 'index',
            name: 'BuyComboIndex',
            meta: { title: '购买套餐', activeMenu: '/setting/index' },
            hidden: true
          }
        ]
      },
      {
        path: 'payment',
        name: 'Payment',
        redirect: '/setting/index',
        meta: { title: '系统信息' },
        hidden: true,
        component: () => import('@/views/setting/buyComDetail'),
        children: [
          {
            path: 'index',
            name: 'BuyComboDetail',
            meta: { title: '支付页面', activeMenu: '/setting/index' },
            hidden: true
          }
        ]
      },
      {
        path: 'order',
        name: 'Order',
        redirect: '/setting/index',
        meta: { title: '系统信息' },
        hidden: true,
        component: () => import('@/views/setting/order'),
        children: [
          {
            path: 'index',
            name: 'OrderIndex',
            meta: { title: '订单', activeMenu: '/setting/index' },
            hidden: true
          }
        ]
      },
      {
        path: 'orderDetail',
        name: 'OrderDetail',
        redirect: '/setting/index',
        meta: { title: '系统信息' },
        hidden: true,
        component: () => import('@/views/setting/orderDetail'),
        children: [
          {
            path: 'index',
            name: 'OrderDetailIndex',
            meta: { title: '订单信息', activeMenu: '/setting/index' },
            hidden: true
          }
        ]
      },
      {
        path: 'orderText',
        name: 'OrderText',
        redirect: '/setting/index',
        meta: { title: '系统信息' },
        hidden: true,
        component: () => import('@/views/setting/orderText'),
        children: [
          {
            path: 'index',
            name: 'OrderTextIndex',
            meta: { title: '订单信息', activeMenu: '/setting/index' },
            hidden: true
          }
        ]
      },
      {
        path: 'inquirySetting',
        name: 'InquirySetting',
        component: () => import('@/views/setting/inquirySetting'),
        meta: { title: '询价设置' }
      },
      {
        path: 'materialSetting',
        name: 'MaterialSetting',
        component: () => import('@/views/setting/materialSetting'),
        meta: { title: '材价库设置' }
      },
      {
        path: 'permission',
        name: 'Permission',
        component: () => import('@/views/setting/permission'),
        meta: { title: '账号权限' }
      },
      {
        path: 'addRole',
        name: 'AddRole',
        component: () => import('@/views/setting/addRole'),
        meta: { title: '添加角色', activeMenu: '/setting/permission' },
        hidden: true
      },
      {
        path: 'editRole',
        name: 'EditRole',
        component: () => import('@/views/setting/editRole'),
        meta: { title: '编辑角色', activeMenu: '/setting/permission' },
        hidden: true
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
