import request from '@/utils/request'

// 获取材料分享列表
export function getMatSharePageList(data) {
  return request({
    url: '/mat/share/getMatSharePageList',
    method: 'post',
    data
  })
}

// 获取分享材价国标分类列表
export function getMatSubClassify(data) {
  return request({
    url: '/mat/share/getMatSubClassify',
    method: 'post',
    data
  })
}

// 查询已有分享的适用时间
export function getShareApplicableTime() {
  return request({
    url: '/mat/share/getShareApplicableTime',
    method: 'post'
  })
}

// 新增分享材料
export function addShareMat(data) {
  return request({
    url: '/mat/share/addShareMat',
    method: 'post',
    data
  })
}

// 删除分享
export function delShareMat(params) {
  return request({
    url: '/mat/share/delShareMat',
    method: 'post',
    params
  })
}

// 撤回分享
export function revokeShareMat(params) {
  return request({
    url: '/mat/share/revokeShareMat',
    method: 'post',
    params
  })
}

// 获取分享材料详情列表
export function getShareMatDetailPageList(data) {
  return request({
    url: '/mat/share/getShareMatDetailPageList',
    method: 'post',
    data
  })
}

// 新增分享材料详细(亦用于分享材价)
export function addShareMatDetail(data) {
  return request({
    url: '/mat/share/addShareMatDetail',
    method: 'post',
    data
  })
}

// 移除分享材料详情
export function delShareMatDetail(params) {
  return request({
    url: '/mat/share/delShareMatDetail',
    method: 'post',
    params
  })
}

// 查询分享材料详情
export function getMatShareDetail(params) {
  return request({
    url: '/mat/share/getMatShareDetail',
    method: 'post',
    params
  })
}

// 导出分享详情excel
export function exportShareDetail(data) {
  return request({
    url: '/mat/share/exportShareDetail',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 查询审核记录
export function getAuditRecord(params) {
  return request({
    url: '/share/audit/getAuditRecord',
    method: 'post',
    params
  })
}

// 提交审核
export function submitAudit(params) {
  return request({
    url: '/share/audit/submitAudit',
    method: 'post',
    params
  })
}

// 审核
export function audit(data) {
  return request({
    url: '/share/audit/audit',
    method: 'post',
    data
  })
}
