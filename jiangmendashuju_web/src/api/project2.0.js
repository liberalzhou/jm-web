import request from '@/utils/request'

// 获取指标树状图
export function getIndicatorTree(data) {
    return request({
        url: '/indicator/project/getIndicatorTree',
        method: 'post',
        data
    })
}

// 获取指标列表
export function getEconListByStageId(data) {
    return request({
        url: '/indicator/project/getEconListByStageId',
        method: 'post',
        data
    })
}

// 通过指标id获取指标详情
export function getDetailsIndicators(params) {
    return request({
        url: '/indicator/project/getDetailsIndicators',
        method: 'post',
        params
    })
}

// 设置无效/有效指标
export function setIndicator(params) {
    return request({
        url: '/indicator/project/setIndicator',
        method: 'post',
        params
    })
}

// 获取指标走势
export function trendIndicator(data) {
    return request({
        url: '/indicator/project/trendIndicator',
        method: 'post',
        data
    })
}

// 导出指标
export function exportIndicator(data) {
    return request({
        url: '/indicator/project/exportIndicator',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

// 体检报告
export function medicalReport(data) {
    return request({
        url: '/project/base/medicalReport',
        method: 'post',
        data
    })
}

// 信息检测
export function informationDetection(data) {
    return request({
        url: '/project/base/informationDetection',
        method: 'post',
        data
    })
}

// 指标预警
export function indicatorWarning(data) {
    return request({
        url: '/project/base/indicatorWarning',
        method: 'post',
        data
    })
}

// 数据检查
export function dataCheck(data) {
    return request({
        url: '/project/base/dataCheck',
        method: 'post',
        data
    })
}

// 下载报告
export function downloadReport(data) {
    return request({
        url: '/project/base/downloadReport',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

// 添加指标预警备注
export function indicatorWarningNotes(params) {
    return request({
        url: '/project/base/indicatorWarningNotes',
        method: 'post',
        params
    })
}
