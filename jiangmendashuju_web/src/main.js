import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import '@/styles/font/font.scss' // 全局字体

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

// 全局确认弹窗
import ConfirmDialog from '@/components/commdialogs/ConfirmDialog'
import CompleteDialog from '@/components/commdialogs/CompleteDialog'

import '@/icons' // svg-icon
import '@/assets/iconfonts/iconfont.css'
import '@/permission' // permission control
import { formatNumber } from '@/utils/common'

import roles from '@/utils/roles' // 路由权限
import hasPermission from './hasPermission' //  按钮权限
Vue.prototype.$roles = roles
Vue.use(hasPermission)

Vue.prototype.formatNumber = formatNumber

// import config from '@/api/config'
// Vue.prototype.$baseUrl = config.baseUrl

// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

Vue.use(ElementUI)

Vue.component('ConfirmDialog', ConfirmDialog)
Vue.component('CompleteDialog', CompleteDialog)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

