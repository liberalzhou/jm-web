const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  loadingTime1: state => state.settings.loadingTime1,
  loadingTime2: state => state.settings.loadingTime2,
  loadingTime3: state => state.settings.loadingTime3,
  loadingTime4: state => state.settings.loadingTime4,
  loadingCommon: state => state.settings.loadingCommon,

  userAccount: state => state.user.userAccount,
  userName: state => state.user.userName,
  userId: state => state.user.userId,
  phone: state => state.user.phone,
  // name: state => state.user.name,
  token: state => state.user.token,
  updateTime: state => state.user.updateTime,
  userHead: state => state.user.userHead,
  department: state => state.user.department,
  position: state => state.user.position,
  qq: state => state.user.qq,
  roles: state => state.user.roles,
  btnRoles: state => state.user.btnRoles,
  permission_routes: state => state.permission.routes, //  路由权限
}
export default getters
