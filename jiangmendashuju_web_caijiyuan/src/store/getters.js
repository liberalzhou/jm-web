const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  userName: state => state.user.userName,
  userAccount: state => state.user.userAccount,
  userHead: state => state.user.userHead,
  token: state => state.user.token,
  loadingTime1: state => state.settings.loadingTime1,
  loadingTime2: state => state.settings.loadingTime2,
  loadingTime3: state => state.settings.loadingTime3,
  loadingTime4: state => state.settings.loadingTime4,
}
export default getters
