import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },
  {
    path: '/uploadMaterial',
    component: Layout,
    redirect: '/uploadMaterial/index',
    meta: { title: '材价采集' },
    children: [{
      path: 'index',
      name: 'UploadMaterial',
      component: () => import('@/views/uploadMaterial/index')
    }, {
      path: 'materialRecord',
      name: 'MaterialRecord',
      meta: { title: '报价记录' },
      component: () => import('@/views/uploadMaterial/materialRecord')
    }, {
      path: 'materialDetail',
      name: 'MaterialDetail',
      meta: { title: '模板详情' },
      component: () => import('@/views/uploadMaterial/materialDetail')
    }]
  },
  {
    path: '/companyInfo',
    component: Layout,
    redirect: '/companyInfo/index',
    children: [{
      path: 'index',
      name: 'CompanyInfo',
      meta: { title: '企业信息' },
      component: () => import('@/views/companyInfo/index')
    }]
  },
  {
    path: '/changePwd',
    component: Layout,
    redirect: '/changePwd/index',
    children: [{
      path: 'index',
      name: 'ChangePwd',
      meta: { title: '修改密码' },
      component: () => import('@/views/companyInfo/changePwd')
    }]
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  { 
    path: '*', 
    redirect: '/404', 
    hidden: true 
  }
]

const createRouter = () => new Router({
  // mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
