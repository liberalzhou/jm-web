import request from '@/utils/request'

// 获取图形验证码
export function authImg() {
  return request({
    url: '/passport/authImg',
    method: 'get',
    responseType: 'arraybuffer'
  })
}

// 登录
export function login(data) {
  return request({
    url: '/collect/messager/loginmain',
    method: 'post',
    data
  })
}
// 退出
export function logout(data) {
  return request({
    url: '/collect/messager/logout',
    method: 'get'
  })
}

// 企业信息资料
export function getCompanyInfo() {
  return request({
    url: '/collect/messager/messagerInfo/toInfoCompletePage',
    method: 'post'
  })
}

// 更新信息员资料
export function updateCollectMessagerInfo(data) {
  return request({
    url: '/collect/messager/messagerInfo/updateCollectMessagerInfo',
    method: 'post',
    data
  })
}

// 获取地区
export function getRegion() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getAddressTree',
    method: 'post'
  })
}

// 获取当前登录用户信息
export function getCurrUser(params) {
  return request({
    url: '/mpcsys/login/getCurrUser',
    method: 'post',
    params
  })
}

// 上传用户头像
export function uploadUserImg(data) {
  return request({
    url: '/sz2sys/userExtend/uploadUserImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data',
    }
  })
}

// 更新账号密码
export function updatePwd(params) {
  return request({
    url: '/sz2sys/userExtend/updatePwd',
    method: 'post',
    params,
  })
}

// 上传营业执照
export function uploadBusinessLicenseImg(data) {
  return request({
    url: '/sz2sys/userExtend/uploadBusinessLicenseImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data',
    }
  })
}