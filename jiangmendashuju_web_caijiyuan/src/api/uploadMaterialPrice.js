import request from '@/utils/request'

// 获取信息员上传材价列表
export function getUploadMaterialPriceList(data) {
  return request({
    url: '/collect/messager/uploadDetail/selectUploadDetail',
    method: 'post',
    data
  })
}

// 上传材价模板下载
export function exportTempFile(params) {
  return request({
    url: '/collect/messager/uploadDetail/downloadTempFile',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 获取材价分类(自定义分类、国际分类)
export function getCjType() {
  return request({
    url: '/mpcmaterail/matprice/getMaterialSort',
    method: 'post'
  })
}

// 获取信息员上传历史记录列表
export function getUploadList(data) {
  return request({
    url: '/collect/messager/collectDetail/getCollectDetailListPage',
    method: 'post',
    data
  })
}

// 信息员上传历史记录导出
export function exportCollectDetailList(data) {
  return request({
    url: '/collect/messager/collectDetail/exportCollectDetailList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 信息员采集记录详细-趋势图数据
export function getAnalyzeTrendData(params) {
  return request({
    url: '/collect/messager/collectDetail/getAnalyzeTrendData',
    method: 'post',
    params
  })
}

// 上传材价
export function uploadTempFile(data) {
  return request({
    url: '/collect/messager/uploadDetail/uploadTempFile',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取模板自定义分类
export function getTempCustomType(params) {
  return request({
    url: '/mpccollect/collectTemplate/getCustomTypeList',
    method: 'get',
    params
  })
}

// 模板清单详情列表
export function getTempDetailList(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/getCollectTemplateDetailListPage',
    method: 'post',
    data
  })
}

// 获取模板反馈类型
export function getFeedbackType(params) {
  return request({
    url: '/collect/messager/uploadDetail/getFeedbackType',
    method: 'get',
    params
  })
}

// 添加模板反馈
export function addFeedback(data) {
  return request({
    url: '/collect/messager/uploadDetail/addFeedback',
    method: 'post',
    data
  })
}
// 上传模板反馈文件
export function uploadFeedFile(data) {
  return request({
    url: '/collect/messager/uploadDetail/uploadFeedFile',
    method: 'post',
    data
  })
}
// 上传材价库的图片
export function uploadMatPriceImg(data) {
  return request({
    url: '/mat/matprice/uploadMatPriceImg',
    method: 'post',
    data
  })
}

// 删除上传材价库的图片
export function deleteMatPriceImg(data) {
  return request({
    url: '/mat/matprice/deleteMatPriceImg',
    method: 'post',
    data
  })
}
