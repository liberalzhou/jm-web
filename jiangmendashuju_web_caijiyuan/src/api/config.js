// API配置
export default {
  // 本地环境
  baseUrl: 'http://localhost:8889'
  // 开发环境
  // baseUrl: 'http://172.16.4.2:8081',
  // 剑枫本地
  // baseUrl: 'http://172.16.10.153:8081',
  // 业举本地
  // baseUrl: 'http://172.16.10.230:8085',

  // 部署测试环境
  // baseUrl: 'http://172.16.4.3:8001/mpc-api',
}
