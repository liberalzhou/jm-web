import request from '@/utils/request'

// 下载失败清单文件
export function downloadErrorExcel(params) {
  return request({
    url: '/sys/common/downloadErrorExcel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 获取类型列表
export function getSysDictList(params) {
  return request({
    url: '/sys/common/getSysDictList',
    method: 'post',
    params
  })
}
