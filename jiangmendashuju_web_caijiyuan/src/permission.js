import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { Message } from 'element-ui'
import getPageTitle from '@/utils/get-page-title'
import { getCurrUser } from '@/api/user'
import { clearStorage } from '@/utils/clearStorage'

NProgress.configure({ showSpinner: false })
const whiteList = ['/login'] // 不重定向白名单

router.beforeEach((to, from, next) => {
  NProgress.start()

  document.title = getPageTitle(to.meta.title)
  if (store.getters.token) {
    if (to.path === '/login') {
      next('/')
    } else {
      var params = {
        userType: '2'
      }
      // 获取用户信息
      getCurrUser(params).then(res => {
        if (res.code == '01' && res.body.user.userAccount) {
          sessionStorage.setItem('user_account', res.body.user.userAccount)
          sessionStorage.setItem('user_name', res.body.user.userName)
          sessionStorage.setItem('user_head', res.body.user.avatarUrl)
          sessionStorage.setItem('token', res.body.token)
          store.commit('user/SET_NAME', res.body.user.userName)
          store.commit('user/SET_USER_ACCOUNT', res.body.user.userAccount)
          store.commit('user/SET_USER_HEAD', res.body.user.avatarUrl)
          store.commit('user/SET_TOKEN', res.body.token)
          next()
        } else {
          Message({
            type: 'warning',
            message: '账户登录失效，请重新登录'
          })
          clearStorage()
          next('/login')
        }
      }).catch(err => {
        Message({
          type: 'warning',
          message: '账户登录失效，请重新登录'
        })
        clearStorage()
        next('/login')
      })
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
