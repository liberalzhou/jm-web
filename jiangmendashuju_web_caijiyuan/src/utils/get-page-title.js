import defaultSettings from '@/settings'

const title = defaultSettings.title || '建设工程造价大数据管理采集平台'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle}`
  }
  return `${title}`
}
