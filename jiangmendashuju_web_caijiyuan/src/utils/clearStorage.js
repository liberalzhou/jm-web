import store from '@/store'

// 清除vuex状态及缓存
export function clearStorage() {
  sessionStorage.removeItem('user_account')
  sessionStorage.removeItem('user_name')
  sessionStorage.removeItem('user_head')
  sessionStorage.removeItem('token')
  store.commit('user/SET_NAME', '')
  store.commit('user/SET_USER_ACCOUNT', '')
  store.commit('user/SET_USER_HEAD', '')
  store.commit('user/SET_TOKEN', '')
}