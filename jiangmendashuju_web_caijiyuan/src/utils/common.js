// 格式化日期YYYY-MM-DD
export function formatDate(curDate, type) {
  if (!curDate) {
      return null
  }
  // 兼容ie，ios浏览器，将YYYY-MM-DD hh:mm:ss转换成YYYY/MM/DD hh:mm:ss格式
  try {
      if (curDate.indexOf('-') > -1 && curDate.indexOf('T') == -1) {
          curDate = curDate.replace(/\-/g, '/')
      }
  } catch(err) {
      console.log('err', curDate)
  }

  try { 
      var date = new Date(curDate)
  } catch(err) {
      return null
  }

  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hours = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()

  if (month < 10) {
      month = '0' + month
  }
  if (day < 10) {
      day = '0' + day
  }
  if (hours < 10) {
      hours = '0' + hours
  }
  if (minutes < 10) {
      minutes = '0' + minutes
  }
  if (seconds < 10) {
      seconds = '0' + seconds
  }
  
  // type = 1 格式返回：YYYY-MM-DD hh:mm:ss
  if (type == '1') {
      return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  }

  return year + '-' + month + '-' + day
}

// 验证手机号是否正确
export function validPhone(phone) {
  var reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  return reg.test(phone)
}

// 验证邮箱是否正确
export function validEmail(email) {
  var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/
  return reg.test(email)
}