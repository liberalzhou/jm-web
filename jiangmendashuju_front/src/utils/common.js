// 格式化日期YYYY-MM-DD
export function formatDate(curDate, type) {
  if (!curDate) {
    return null
  }
  // 兼容ie，ios浏览器，将YYYY-MM-DD转换成YYYY/MM/DD格式
  try {
    if (curDate.indexOf('-') > -1 && curDate.indexOf('T') == -1) {
      curDate = curDate.replace(/\-/g, '/')
      curDate = curDate.replace('.0', '')// 这里是将".0" 去掉，因为.0依旧会影响ios对日期的解析
    }
  } catch (err) {
    // console.log('err', curDate)
  }

  try {
    var date = new Date(curDate)
  } catch (err) {
    return null
  }

  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hours = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()

  if (month < 10) {
    month = '0' + month
  }
  if (day < 10) {
    day = '0' + day
  }
  if (hours < 10) {
    hours = '0' + hours
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (seconds < 10) {
    seconds = '0' + seconds
  }

  // type = 1 格式返回：YYYY-MM-DD hh:mm:ss
  if (type == '1') {
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  }
  // type = 2 格式返回：YYYY-MM-DD
  if (type == '2') {
    return year + '-' + month + '-' + day
  }
  // type = 3 格式返回：YYYY年MM月DD日
  if (type == '3') {
    return year + '年' + month + '月' + day + '日'
  }
  // type = 4 格式返回：YYYY年MM月
  if (type == '4') {
    return year + '年' + month + '月'
  }

  return year + '-' + month
}

// 验证手机号是否正确
export function validPhone(phone) {
  var reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  return reg.test(phone)
}

// 验证邮箱是否正确
export function validEmail(email) {
  var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/
  return reg.test(email)
}

// 数值输入限制
export function checkNumber(price) {
  // 清除"数字"和"."以外的字符
  price = price.replace(/[^\d.]/g, '')
  // 验证第一个字符是数字而不是字符
  price = price.replace(/^\./g, '')
  // 只保留第一个.清除多余的
  price = price.replace(/\.{2,}/g, '.')
  price = price.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.')
  // 只能输入两个小数
  price = price.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3')
  return price
}

/**
 * 数值以,分隔(例：12,345,678.90)
 * @param {number/string} value
 * @param {boolean} hasUnit 该字段为true，则代表结尾可能带有单位，即非数字的字符（例：99.9元/m）
 * @return {string}
 */
export function formatNumber(value, hasUnit) {
  let curValue = ''
  // 结尾非数值字符串
  let endStr = ''
  if (hasUnit) {
    let curValueArr = []
    try {
      curValueArr = value.split('')
    } catch (err) {}
    for (let i = 0; i < curValueArr.length; i++) {
      if (curValueArr[i] == '.' || !isNaN(Number(curValueArr[i]))) {
        curValue += curValueArr[i]
      } else {
        endStr = value.substring(i)
        break
      }
    }
  } else {
    curValue = value
  }
  if (isNaN(Number(curValue))) {
    return null
  }
  let curNumber = ''
  try {
    curNumber = curValue.toString()
  } catch (err) {
    return
  }

  // 整数部分
  const intSum = curNumber.split('.')[0].replace(/\B(?=(?:\d{3})+$)/g, ',')
  // 小数位
  let decimal = null
  if (curNumber.split('.')[1]) {
    decimal = curNumber.split('.')[1]
  }

  let res = intSum
  if (decimal) {
    res += '.' + decimal
  }
  if (hasUnit) {
    return res + endStr
  }

  return res
}
