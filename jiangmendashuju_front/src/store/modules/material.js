const moduleMaterial = {
    state: {
        marketPriceList: [],  // 市场价
        infoPriceList: [],  // 信息价
        synthePriceList: [],  // 综合价
        checkPriceInfo: null,  // 材价信息
    },
    mutations: {
        SET_MARKET_PRICE_LIST(state, data) {
            state.marketList = data;
        },
        SET_INFO_PRICE_LIST(state, data) {
            state.infoList = data;
        },
        SET_SYNTHE_PRICE_LIST(state, data) {
            state.synthePriceList = data;
        },
        SET_CHECK_PRICE_INFO(state, data) {
            state.checkPriceInfo = data;
        }
    },
    getters: {
        marketPriceList(state) {
            return state.marketPriceList;
        },
        infoPriceList(state) {
            return state.infoPriceList;
        },
        synthePriceList(state) {
            return state.synthePriceList;
        },
        checkPriceInfo(state) {
            return state.checkPriceInfo;
        }
    }
}

export default moduleMaterial