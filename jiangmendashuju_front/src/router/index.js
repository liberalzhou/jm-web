import Layout from '@/layout'
import menuLayout from '@/layout/menu'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/publicHome',
    children: [{
      path: 'publicHome',
      name: 'publicHome',
      component: () => import('@/views/mainSystem/publicHome/index'),
      meta: { system: ['main'], title: '进入页', icon: 'iconfont icon-shouye' },
      hidden: true
    }]
  },
  {
    path: '/profile',
    component: Layout,
    children: [{
      path: 'index',
      name: 'profile',
      component: () => import('@/views/mainSystem/profile/index'),
      meta: { title: '个人信息', icon: 'iconfont icon-yonghuguanli' },
      hidden: true
    }]
  },
  {
    path: '/changePassword',
    component: Layout,
    children: [{
      path: 'index',
      name: 'changePassword',
      component: () => import('@/views/mainSystem/changePassword/index'),
      meta: { title: '修改密码', icon: 'iconfont icon-gaimima' },
      hidden: true
    }]
  }
]
/**
 * meta.system 代表路由所属系统，路由可能会被多个系统共用
*/
export const asyncRoutes = [
// mainSystem
  {
    path: '/accountSettings',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:getAccountPageList' },
    children: [{
      path: 'index',
      name: 'accountSettings',
      component: () => import('@/views/mainSystem/accountSettings/index'),
      meta: { system: ['main'], menuKey: 'sys:getAccountPageList', SYSTEM_FLAG: '1000', title: '账号设置', icon: 'iconfont icon-zhanghaoshezhi' }
    }]
  },
  {
    path: '/userControl',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:getUserPageList' },
    children: [{
      path: 'index',
      name: 'userControl',
      component: () => import('@/views/mainSystem/userControl/index'),
      meta: { system: ['main'], menuKey: 'sys:getUserPageList', SYSTEM_FLAG: '1000', title: '用户管理', icon: 'iconfont icon-yonghuguanli' }
    }]
  },
  {
    path: '/messageTemplate',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:template:listPage' },
    children: [{
      path: 'index',
      name: 'messageTemplate',
      component: () => import('@/views/mainSystem/messageTemplate/index'),
      meta: { system: ['main'], menuKey: 'sys:template:listPage', SYSTEM_FLAG: '1000', title: '通知模板', icon: 'iconfont icon-mobanguanli' }
    }]
  },
  {
    path: '/rolePermission',
    component: Layout,
    redirect: '/rolePermission/index',
    meta: { system: ['main'], menuKey: 'sys:getRoleList' },
    children: [{
      path: 'index',
      name: 'rolePermission',
      component: () => import('@/views/mainSystem/rolePermission/index'),
      meta: { system: ['main'], menuKey: 'sys:getRoleList', SYSTEM_FLAG: '1000', title: '角色权限', icon: 'iconfont icon-jiaosequanxian' }
    },
    {
      path: 'addRole',
      name: 'AddRole',
      component: () => import('@/views/mainSystem/rolePermission/addRole'),
      meta: { system: ['main'], menuKey: 'sys:addRole', SYSTEM_FLAG: '1000', title: '添加角色' },
      hidden: true
    },
    {
      path: 'editRole',
      name: 'EditRole',
      component: () => import('@/views/mainSystem/rolePermission/editRole'),
      meta: { system: ['main'], menuKey: 'sys:editRole', SYSTEM_FLAG: '1000', title: '编辑角色', activeMenu: '/rolePermission/index' },
      hidden: true
    }
    ]
  },
  {
    path: '/systemSettings',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:template:listPage' },
    children: [{
      path: 'index',
      name: 'systemSettings',
      component: () => import('@/views/mainSystem/systemSettings/index'),
      meta: { system: ['main'], menuKey: 'sys:template:listPage', SYSTEM_FLAG: '1000', title: '系统设置', icon: 'iconfont icon-zhanghaoshezhi' }
    }]
  },
  // 暂时不要
  // {
  //   path: '/installConfig',
  //   component: Layout,
  //   children: [{
  //     path: 'index',
  //     name: 'installConfig',
  //     component: () => import('@/views/installConfig/index'),
  //     meta: { title: '初始化页面配置', icon: 'iconfont icon-shezhi' }
  //   }]
  // },
  // 暂时不要

  // 咨询公司
  {
    path: '/project',
    component: Layout,
    redirect: '/project/index',
    meta: { system: ['zixun'], menuKey: 'index', title: '项目管理', icon: 'iconfont icon-xiangmuguanli' },
    children: [
      {
        path: 'detail',
        name: 'Detail',
        component: () => import('@/views/project/detail'),
        meta: { system: ['zixun'], menuKey: 'index', title: '项目详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'index',
        name: 'Projects',
        component: () => import('@/views/project/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '项目库' }
      },
      {
        path: 'importProject',
        name: 'ImportProject',
        component: () => import('@/views/project/importProject'),
        meta: { system: ['zixun'], menuKey: 'index', title: '导入项目' }
      },
      {
        path: 'compareProject',
        name: 'CompareProject',
        component: () => import('@/views/project/compareProject'),
        // component: () => import('@/views/project/projectComparison'),
        meta: { system: ['zixun'], menuKey: 'index', title: '项目对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'compareStage',
        name: 'compareStage',
        component: () => import('@/views/project/compareStage'),
        // component: () => import('@/views/project/wholeComparison'),
        meta: { system: ['zixun'], menuKey: 'index', title: '阶段对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'projectVisible',
        name: 'ProjectVisible',
        component: () => import('@/views/project/projectVisible'),
        meta: { system: ['zixun'], menuKey: 'index', title: '可视化', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'report',
        name: 'Peport',
        component: () => import('@/views/project/testing/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '体检报告', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'testing',
        name: 'Testing',
        component: () => import('@/views/project/testing/doing'),
        meta: { system: ['zixun'], menuKey: 'index', title: '项目体检', activeMenu: '/project/index' },
        hidden: true
      }
    ]
  },
  {
    path: '/target',
    component: Layout,
    redirect: '/target/index',
    meta: { system: ['zixun'], menuKey: 'index', title: '指标管理', icon: 'iconfont icon-zhibiaoguanli' },
    children: [
      {
        path: 'index',
        name: 'Targets',
        component: () => import('@/views/target/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '指标库' }
      },
      {
        path: 'synthesize',
        name: 'synthesize',
        component: () => import('@/views/target/synthesizeIndex'),
        meta: { system: ['zixun'], menuKey: 'index', title: '综合指标库' }
      },
      {
        path: 'synthesizeExamine',
        name: 'SynthesizeExamine',
        component: () => import('@/views/target/synthesizeExamine'),
        meta: { system: ['zixun'], menuKey: 'index', title: '综合指标审核' }
      },
      {
        path: 'detail',
        name: 'TargetDetail',
        component: () => import('@/views/target/detail/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '指标库详情', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'calculate',
        name: 'Calculate',
        component: () => import('@/views/target/indexCalculation'),
        meta: { system: ['zixun'], menuKey: 'index', title: '指标计算', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'analysis',
        name: 'Analysis',
        component: () => import('@/views/target/indexAnalysis'),
        meta: { system: ['zixun'], menuKey: 'index', title: '指标分析', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'synthesizeDetail',
        name: 'SynthesizeDetail',
        component: () => import('@/views/target/detail/synthesizeCalculate'),
        meta: { system: ['zixun'], menuKey: 'index', title: '综合指标库详情', activeMenu: '/target/synthesize' },
        hidden: true
      }
    ]
  },
  {
    path: '/targetApplication',
    component: Layout,
    redirect: '/targetApplication/index',
    meta: { system: ['zixun'], menuKey: 'index', title: '指标应用', icon: 'iconfont icon-zhibiaoyingyong' },
    // meta: { title: '新项目智能应用', icon: 'iconfont icon-zhibiaoyingyong' },
    children: [
      {
        path: 'index',
        name: 'TargetCost',
        component: () => import('@/views/targetApplication/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '快速造价' }
      },
      {
        path: 'newCalcula',
        name: 'NewCalcula',
        component: () => import('@/views/targetApplication/newCalculation'),
        meta: { system: ['zixun'], menuKey: 'index', title: '快速造价', activeMenu: '/targetApplication/index' },
        hidden: true
      },
      {
        path: 'calculaDetail',
        name: 'CalculaDetail',
        component: () => import('@/views/targetApplication/calculaDetail'),
        meta: { system: ['zixun'], menuKey: 'index', title: '测算方案详情', activeMenu: '/targetApplication/index' },
        hidden: true
      },
      {
        path: 'targetReal',
        name: 'TargetReal',
        component: () => import('@/views/targetApplication/targetRealtime'),
        meta: { system: ['zixun'], menuKey: 'index', title: '实时指标' }
      },
      {
        path: 'targetTest',
        name: 'TargetTest',
        component: () => import('@/views/targetApplication/targetTesting'),
        meta: { system: ['zixun'], menuKey: 'index', title: '智能检测' }
      }
    ]
  },
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/index',
    name: 'setting',
    // meta: { system: ['zixun'], menuKey: 'index', title: '设置中心', icon: 'iconfont icon-zhanghaoshezhi' },
    children: [
      {
        path: 'inquirySetting',
        name: 'InquirySetting',
        component: () => import('@/views/setting/inquirySetting'),
        meta: { system: ['zixun'], menuKey: 'index', title: '询价设置' }
      },
      {
        path: 'materialSetting',
        name: 'MaterialSetting',
        component: () => import('@/views/setting/materialSetting'),
        meta: { system: ['zixun'], menuKey: 'index', title: '材价设置' }
      },
      {
        path: '/projectSet',
        component: menuLayout,
        redirect: '/projectSet/costStage',
        // alwaysShow: true,
        // hidden: true,
        meta: { system: ['zixun'], menuKey: 'index', title: '项目库设置' },
        // meta: { title: '项目库设置', icon: 'iconfont icon-zbk1' },
        children: [
          {
            path: 'costStage',
            name: 'CostStage',
            component: () => import('@/views/projectSet/costStage'),
            meta: { system: ['zixun'], menuKey: 'index', title: '造价阶段标准配置' }
          },
          {
            path: 'unitProject',
            name: 'UnitProject',
            component: () => import('@/views/projectSet/unitProject'),
            meta: { system: ['zixun'], menuKey: 'index', title: '单位工程标准表配置' }
          },
          {
            path: 'branchProject',
            name: 'BranchProject',
            component: () => import('@/views/projectSet/branchProject'),
            meta: { system: ['zixun'], menuKey: 'index', title: '分部工程标准表配置', activeMenu: '/projectSet/unitProject' },
            hidden: true
          },
          {
            path: 'skillFeature',
            name: 'SkillFeature',
            component: () => import('@/views/projectSet/skillFeature'),
            meta: { system: ['zixun'], menuKey: 'index', title: '技术特征配置' }
          },
          {
            path: 'quotaStandard',
            name: 'QuotaStandard',
            component: () => import('@/views/projectSet/quotaStandard'),
            meta: { system: ['zixun'], menuKey: 'index', title: '造价定额规范配置' }
          }
        ]
      },
      {
        path: '/targetSet',
        name: 'targetSet',
        redirect: '/targetSet/earlyWarn',
        component: menuLayout,
        meta: { system: ['zixun'], menuKey: 'index', title: '指标设置' },
        // meta: { title: '指标设置', icon: 'iconfont icon-zhibiaokushezhi' },

        children: [
          {
            path: 'earlyWarn',
            name: 'EarlyWarn',
            component: () => import('@/views/targetSet/earlyWarning'),
            meta: { system: ['zixun'], menuKey: 'index', title: '预警设置' }
          },
          {
            path: 'addWarn',
            name: 'AddWarn',
            component: () => import('@/views/targetSet/operates/addEarlyWarn'),
            meta: { system: ['zixun'], menuKey: 'index', title: '配置预警指标', activeMenu: '/targetSet/earlyWarn' },
            hidden: true
          },
          {
            path: 'economicTarget',
            name: 'EconomicTarget',
            component: () => import('@/views/targetSet/economicTarget'),
            meta: { system: ['zixun'], menuKey: 'index', title: '实时经济指标配置' }
          },
          {
            path: 'addEconomic',
            name: 'AddEconomic',
            component: () => import('@/views/targetSet/operates/addEconomicTarget'),
            meta: { system: ['zixun'], menuKey: 'index', title: '配置实时经济指标', activeMenu: '/targetSet/economicTarget' },
            hidden: true
          },
          {
            path: 'technologyTarget',
            name: 'TechnologyTarget',
            component: () => import('@/views/targetSet/technologyTarget'),
            meta: { system: ['zixun'], menuKey: 'index', title: '实时技术指标配置' }
          },
          {
            path: 'addTechnology',
            name: 'AddTechnology',
            component: () => import('@/views/targetSet/operates/addTechnologyTarget'),
            meta: { system: ['zixun'], menuKey: 'index', title: '配置实时技术指标', activeMenu: '/targetSet/technologyTarget' },
            hidden: true
          }
          // {
          //   path: 'economicConfigure',
          //   name: 'EconomicConfigure',
          //   component: () => import('@/views/targetSet/economicConfigure'),
          //   meta: { title: '经济指标生成配置' }
          // },
          // {
          //   path: 'technologyConfigure',
          //   name: 'TechnologyConfigure',
          //   component: () => import('@/views/targetSet/technologyConfigure'),
          //   meta: { title: '技术指标生成配置' }
          // },
        ]
      }

      // {
      //   path: 'permission',
      //   name: 'Permission',
      //   component: () => import('@/views/setting/permission'),
      //   meta: { title: '账号设置' }
      // },
      // {
      //   path: 'addRole',
      //   name: 'AddRole',
      //   component: () => import('@/views/setting/addRole'),
      //   meta: { title: '添加角色', activeMenu: '/setting/permission' },
      //   hidden: true
      // },
      // {
      //   path: 'editRole',
      //   name: 'EditRole',
      //   component: () => import('@/views/setting/editRole'),
      //   meta: { title: '编辑角色', activeMenu: '/setting/permission' },
      //   hidden: true
      // },
      // {
      //   path: 'index',
      //   name: 'System',
      //   component: () => import('@/views/setting/index'),
      //   meta: { title: '系统设置' }
      // },
      // {
      //   path: 'buyCombo',
      //   name: 'BuyCombo',
      //   redirect: '/setting/index',
      //   meta: { title: '系统设置' },
      //   hidden: true,
      //   component: () => import('@/views/setting/buyCombo'),
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'BuyComboIndex',
      //       meta: { title: '购买套餐', activeMenu: '/setting/index' },
      //       hidden: true
      //     }
      //   ]
      // },
      // {
      //   path: 'payment',
      //   name: 'Payment',
      //   redirect: '/setting/index',
      //   meta: { title: '系统设置' },
      //   hidden: true,
      //   component: () => import('@/views/setting/buyComDetail'),
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'BuyComboDetail',
      //       meta: { title: '支付页面', activeMenu: '/setting/index' },
      //       hidden: true
      //     }
      //   ]
      // },
      // {
      //   path: 'order',
      //   name: 'Order',
      //   redirect: '/setting/index',
      //   meta: { title: '系统设置' },
      //   hidden: true,
      //   component: () => import('@/views/setting/order'),
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'OrderIndex',
      //       meta: { title: '订单', activeMenu: '/setting/index' },
      //       hidden: true
      //     }
      //   ]
      // },
      // {
      //   path: 'orderDetail',
      //   name: 'OrderDetail',
      //   redirect: '/setting/index',
      //   meta: { title: '系统设置' },
      //   hidden: true,
      //   component: () => import('@/views/setting/orderDetail'),
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'OrderDetailIndex',
      //       meta: { title: '订单信息', activeMenu: '/setting/index' },
      //       hidden: true
      //     }
      //   ]
      // },
      // {
      //   path: 'orderText',
      //   name: 'OrderText',
      //   redirect: '/setting/index',
      //   meta: { title: '系统设置' },
      //   hidden: true,
      //   component: () => import('@/views/setting/orderText'),
      //   children: [
      //     {
      //       path: 'index',
      //       name: 'OrderTextIndex',
      //       meta: { title: '订单信息', activeMenu: '/setting/index' },
      //       hidden: true
      //     }
      //   ]
      // }
    ]
  },
  // {
  //   path: '/inquiryManage',
  //   component: Layout,
  //   name: 'Inquiry',
  //   redirect: '/inquiryManage/index',
  //   meta: { menuKey: 'inquire', SYSTEM_FLAG: '1002', title: '询价管理', icon: 'iconfont icon-xunjiaguanli' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Inquiry',
  //       component: () => import('@/views/inquiryManage/index'),
  //       meta: { menuKey: 'inquire:manage:getInquireList', SYSTEM_FLAG: '1002', title: '询价库' }
  //     },
  //     {
  //       path: 'toInquiry',
  //       name: 'ToInquiry',
  //       component: () => import('@/views/inquiryManage/toInquiry'),
  //       meta: { title: '询价', activeMenu: '/inquiryManage/index', SYSTEM_FLAG: '1002' },
  //       hidden: true
  //     },
  //     { // 为了让侧边栏选中询价审核
  //       path: 'editInquiry',
  //       name: 'EditInquiry',
  //       component: () => import('@/views/inquiryManage/toInquiry'),
  //       meta: { title: '编辑询价', SYSTEM_FLAG: '1002', activeMenu: '/inquiryManage/inquiryExamine' },
  //       hidden: true
  //     },
  //     {
  //       path: 'inquiryDetails',
  //       name: 'InquiryDetails',
  //       component: () => import('@/views/inquiryManage/inquiryDetails'),
  //       meta: { title: '项目批次详情', activeMenu: '/inquiryManage/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'inquiryExamine',
  //       name: 'InquiryExamine',
  //       component: () => import('@/views/inquiryManage/inquiryExamine'),
  //       meta: { title: '询价审核' }
  //     },
  //     {
  //       path: 'examinePass',
  //       name: 'ExaminePass',
  //       component: () => import('@/views/inquiryManage/examinePass'),
  //       meta: { title: '审核通过数据', activeMenu: '/inquiryManage/inquiryExamine' },
  //       hidden: true
  //     },
  //     {
  //       path: 'examineFail',
  //       name: 'ExamineFail',
  //       component: () => import('@/views/inquiryManage/examineFail'),
  //       meta: { title: '审核不通过数据', activeMenu: '/inquiryManage/inquiryExamine' },
  //       hidden: true
  //     },
  //     {
  //       path: 'examinePend',
  //       name: 'ExaminePend',
  //       component: () => import('@/views/inquiryManage/examinePend'),
  //       meta: { title: '待审核数据', activeMenu: '/inquiryManage/inquiryExamine' },
  //       hidden: true
  //     },
  //     {
  //       path: 'inquiryDrafts',
  //       name: 'InquiryDrafts',
  //       component: () => import('@/views/inquiryManage/inquiryDrafts'),
  //       meta: { title: '草稿箱' }
  //     },
  //     { // 为了让侧边栏选中草稿箱
  //       path: 'toInquiryDrafts',
  //       name: 'ToInquiryDrafts',
  //       component: () => import('@/views/inquiryManage/toInquiry'),
  //       meta: { title: '询价', activeMenu: '/inquiryManage/inquiryDrafts' },
  //       hidden: true
  //     }
  //   ]
  // },

  // {
  //   path: '/materialManage',
  //   component: Layout,
  //   redirect: '/materialManage/index',
  //   alwaysShow: true,
  //   meta: { menuKey: 'to_mpc_home', SYSTEM_FLAG: '1003', title: '材价管理', icon: 'iconfont icon-caijiaguanli12' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Material',
  //       component: () => import('@/views/materialManage/index'),
  //       meta: { menuKey: 'to_mpc_home', SYSTEM_FLAG: '1003', title: '材价库' }
  //     },
  //     {
  //       path: 'checkMaterial',
  //       name: 'CheckMaterial',
  //       component: () => import('@/views/materialManage/checkMaterial'),
  //       meta: { menuKey: 'to_mpc_home', SYSTEM_FLAG: '1003', title: '造价通查价' }
  //     },
  //     {
  //       path: 'sameMaterial',
  //       name: 'SameMaterial',
  //       component: () => import('@/views/materialManage/sameMaterial'),
  //       meta: { title: '相同材价', activeMenu: '/materialManage/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'checkResult',
  //       name: 'CheckResult',
  //       component: () => import('@/views/materialManage/detail/checkResult'),
  //       meta: { title: '查价结果', activeMenu: '/materialManage/checkMaterial' },
  //       hidden: true
  //     },
  //     {
  //       path: 'checkHistory',
  //       name: 'CheckHistory',
  //       component: () => import('@/views/materialManage/detail/checkHistory'),
  //       meta: { title: '查价历史', activeMenu: '/materialManage/checkMaterial' },
  //       hidden: true
  //     }
  //   ]
  // },

  // {
  //   path: '/shareManage',
  //   component: Layout,
  //   redirect: '/shareManage/index',
  //   meta: { title: '分享管理', icon: 'iconfont icon-fenxiangguanli' },
  //   hidden: true, // czhui
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Share',
  //       component: () => import('@/views/shareManage/index'),
  //       meta: { title: '分享库' }
  //     },
  //     {
  //       path: 'shareExamine',
  //       name: 'ShareExamine',
  //       component: () => import('@/views/shareManage/shareExamine'),
  //       meta: { title: '分享审核' }
  //     },
  //     {
  //       path: 'shareDetail',
  //       name: 'ShareDetail',
  //       component: () => import('@/views/shareManage/shareDetail'),
  //       meta: { title: '分享详情', activeMenu: '/shareManage/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'shareEdit',
  //       name: 'ShareEdit',
  //       component: () => import('@/views/shareManage/shareEdit'),
  //       meta: { title: '编辑分享', activeMenu: '/shareManage/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'shareAdd',
  //       name: 'ShareAdd',
  //       component: () => import('@/views/shareManage/shareAdd'),
  //       meta: { title: '添加分享', activeMenu: '/shareManage/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'shareExamineDetail',
  //       name: 'ShareExamineDetail',
  //       component: () => import('@/views/shareManage/shareExamineDetail'),
  //       meta: { title: '分享审核详情', activeMenu: '/shareManage/shareExamine' },
  //       hidden: true
  //     }
  //   ]
  // },

  // /*v2.0没有这两个，先屏蔽*/
  // {
  //   path: '/supplier',
  //   component: Layout,
  //   redirect: '/supplier/index',
  //   meta: { title: '供应商管理', icon: 'iconfont icon-zhibiaoku11' },
  //   hidden: true,  // czhui
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Supplier',
  //       component: () => import('@/views/supplier/index'),
  //       meta: { title: '供应商库' },
  //     },
  //     {
  //       path: 'brandIndex',
  //       name: 'BrandIndex',
  //       component: () => import('@/views/supplier/brandIndex'),
  //       meta: { title: '品牌库' },
  //     },
  //     {
  //       path: 'addSupplier',
  //       name: 'AddSupplier',
  //       component: () => import('@/views/supplier/detail/addSupplier'),
  //       meta: { title: '添加供应商', activeMenu: '/supplier/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'detail',
  //       name: 'supplierDetail',
  //       component: () => import('@/views/supplier/detail/supplierDetail'),
  //       meta: { title: '供应商详情', activeMenu: '/supplier/index' },
  //       hidden: true
  //     },
  //     {
  //       path: 'brandDetail',
  //       name: 'BrandDetail',
  //       component: () => import('@/views/supplier/detail/brandDetail'),
  //       meta: { title: '品牌库详情', activeMenu: '/supplier/brandIndex' },
  //       hidden: true
  //     },
  //   ]
  // },

  {
    path: '/file',
    component: Layout,
    redirect: '/file/index',
    meta: { system: ['zixun'], menuKey: 'index', title: '文档管理', icon: 'iconfont icon-wendangguanli' },
    // hidden: true,  // czhui
    children: [
      {
        path: 'index',
        name: 'File',
        component: () => import('@/views/file/index'),
        meta: { system: ['zixun'], menuKey: 'index', title: '公共文档' }
      },
      {
        path: 'indexDetail',
        name: 'IndexDetail',
        component: () => import('@/views/file/detail/indexDetail'),
        meta: { system: ['zixun'], menuKey: 'index', title: '材价文档详情', activeMenu: '/file/index' },
        hidden: true
      },
      {
        path: 'projectDetail',
        name: 'ProjectDetail',
        component: () => import('@/views/file/detail/projectDetail'),
        meta: { system: ['zixun'], menuKey: 'index', title: '项目文档详情', activeMenu: '/file/index' },
        hidden: true
      },
      {
        path: 'businessDetail',
        name: 'BusinessDetail',
        component: () => import('@/views/file/detail/businessDetail'),
        meta: { system: ['zixun'], menuKey: 'index', title: '企业文档详情', activeMenu: '/file/index' },
        hidden: true
      },
      {
        path: 'privateIndex',
        name: 'privateIndex',
        component: () => import('@/views/file/components/CommPage'),
        meta: { system: ['zixun'], menuKey: 'index', title: '私人文档' },
        props: { sourceChannel: 4, hasBack: false }
      },
      {
        path: 'informationIndex',
        name: 'informationIndex',
        component: () => import('@/views/file/components/CommPage'),
        meta: { system: ['zixun'], menuKey: 'index', title: '资讯中心' },
        props: { sourceChannel: 5, hasBack: false }
      }
    ]
  },
  // {
  //   path: '/personal',
  //   component: Layout,
  //   redirect: '/personal/index',
  //   meta: { title: '个人中心', icon: 'iconfont icon-ger1' },
  //   // hidden: true,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'UserInfo',
  //       component: () => import('@/views/personal/index'),
  //       meta: { title: '个人设置' }
  //     },
  //   ]
  // },

  // 采算编发
  {
    path: '/cs/dashboard',
    redirect: '/cs/dashboard/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home' },
    children: [{
      path: 'index',
      name: 'cs/Dashboard',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '首页', icon: 'iconfont icon-shouye' },
      component: () => import('@/views/cs/dashboard/index')
    }]
  },
  {
    path: '/cs/modeManagement',
    redirect: '/cs/modeManagement/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '模板管理', icon: 'iconfont icon-mobanguanli' },
    children: [{
      path: 'index',
      name: 'modeManagement',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '模板管理', breadcrumb: false, activeMenu: '/cs/modeManagement/index' },
      component: () => import('@/views/cs/modeManagement/index')
    },
    {
      path: 'modeDetail',
      name: 'modeDetail',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '模板库详情', activeMenu: '/cs/modeManagement/index' },
      component: () => import('@/views/cs/modeManagement/modeDetail'),
      hidden: true
    }]
  },
  {
    path: '/cs/dataCollection',
    redirect: '/cs/dataCollection/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home' },
    children: [{
      path: 'index',
      name: 'dataCollection',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '数据采集提取', icon: 'iconfont icon-shujucaijitiqu' },
      component: () => import('@/views/cs/dataCollection/index')
    }]
  },
  {
    path: '/cs/EvaluationPrice',
    redirect: '/cs/EvaluationPrice/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '测算价评审' },
    children: [{
      path: 'index',
      name: 'EvaluationPrice',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '测算价评审', icon: 'iconfont icon-cesuanjiapingshen', breadcrumb: false, activeMenu: '/cs/EvaluationPrice/index' },
      component: () => import('@/views/cs/EvaluationPrice/index')
      // hidden: true
    },
    {
      path: 'EvaluationPriceDetail',
      name: 'EvaluationPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '评审详情', activeMenu: '/cs/EvaluationPrice/index' },
      component: () => import('@/views/cs/EvaluationPrice/EvaluationPriceDetail'),
      hidden: true
    }
    ]
  },
  {
    path: '/cs/Information',
    redirect: '/cs/Information/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '信息价' },
    children: [{
      path: 'index',
      name: 'Information',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '信息价', icon: 'iconfont icon-xinxijia', breadcrumb: false, activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/index')
      // hidden: true
    },
    {
      path: 'infoPriceDetail',
      name: 'infoPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '信息价详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/infoPriceDetail'),
      hidden: true
    },
    {
      path: 'historyPriceDetail',
      name: 'historyPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '历史库详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/historyPriceDetail'),
      hidden: true
    },
    {
      path: 'recycleBinDetail',
      name: 'recycleBinDetail',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '回收站信息价详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/recycleBinDetail'),
      hidden: true
    }
    ]
  },

  {
    path: '/cs/releaseManagement',
    component: Layout,
    redirect: '/cs/releaseManagement/periodManager',
    // alwaysShow: true,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '发布管理' },
    children: [
      {
        path: 'periodManager',
        name: 'periodManager',
        // meta: { menuKey: 'to_mpc_home', title: '期刊管理' },
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '发布管理', icon: 'iconfont icon-fabuguanli', breadcrumb: false, activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/periodManager')
        // hidden: true
      },
      {
        path: 'PeriodicalDetail',
        name: 'PeriodicalDetail',
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '期刊详情', activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/periodicalDetail'),
        hidden: true
      },
      {
        path: 'issueRecordDetail',
        name: 'issueRecordDetail',
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '发布记录信息价详情', activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/issueRecordDetail'),
        hidden: true
      }
    ]
  },
  {
    path: '/cs/materialManage',
    component: Layout,
    redirect: '/cs/materialManage',
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home' },
    children: [
      {
        path: 'UserManage',
        name: 'UserManage',
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '采集员管理', icon: 'iconfont icon-caijiyuanguanli' },
        component: () => import('@/views/cs/materialManage/userManage')
      }
    ]
  },
  {
    path: '/cs/appraiseExamine',
    redirect: '/cs/appraiseExamine/materialCount',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '评价考核', icon: 'iconfont icon-mobanguanli' },
    children: [
      {
        path: 'materialCount',
        name: 'materialCount',
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '评价考核', breadcrumb: false, activeMenu: '/cs/appraiseExamine/materialCount' },
        component: () => import('@/views/cs/appraiseExamine/materialCount')
      },
      {
        path: 'materialCountDetail',
        component: menuLayout,
        redirect: '/cs/appraiseExamine/materialCountDetail/index',
        meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '评价考核详情', activeMenu: '/cs/appraiseExamine/materialCount' },
        hidden: true,
        children: [
          {
            path: 'index',
            name: 'materialCountDetail',
            meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '评价考核详情', breadcrumb: false, activeMenu: '/cs/appraiseExamine/materialCount' },
            component: () => import('@/views/cs/appraiseExamine/materialCountDetail')
          },
          {
            path: 'materialDetail',
            name: 'materialDetail',
            meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '材价详情', activeMenu: '/cs/appraiseExamine/materialCount' },
            component: () => import('@/views/cs/appraiseExamine/materialDetail')
          }]
      }]
  },
  {
    path: '/cs/setting',
    name: 'HomeSet',
    component: Layout,
    redirect: '/cs/setting/modeSet',
    meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '参数设置', icon: 'iconfont icon-zhanghaoshezhi' },
    children: [{
      path: 'homeSet',
      name: 'HomeSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '首页统计设置' },
      component: () => import('@/views/cs/setting/homeSet')
    },
    {
      path: 'modeSet',
      name: 'ModeSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '模板清单设置' },
      component: () => import('@/views/cs/setting/modeSet')
    },
    {
      path: 'synthesizeSet',
      name: 'SynthesizeSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '测算设置' }, // 旧：综合价设置
      component: () => import('@/views/cs/setting/synthesizeSet')
    },
    {
      path: 'infoSet',
      name: 'InfoSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '数据采集配置周期' }, // 旧：信息价设置
      component: () => import('@/views/cs/setting/infoSet')
    },
    {
      path: 'periodicalSet',
      name: 'PeriodicalSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '发布管理设置' }, // 旧：期刊设置
      component: () => import('@/views/cs/setting/periodicalSet')
    },
    {
      path: 'userSet',
      name: 'UserSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '企业类型设置' }, // 旧：采集员设置
      component: () => import('@/views/cs/setting/userSet')
    },
    {
      path: 'materialSet',
      name: 'MaterialSet',
      meta: { system: ['caisuan'], menuKey: 'to_mpc_home', title: '材价设置' },
      component: () => import('@/views/cs/setting/materialSet')
    }
    // { // 其他设置还没配权限
    //   path: 'otherSet',
    //   name: 'otherSet',
    //   meta: { menuKey: 'to_mpc_home', title: '其他设置' },
    //   component: () => import('@/views/cs/setting/otherSet')
    // }
    ]
  },
  // ------------------------------------------------------------++++++++++++++++++++++++++++++++++++++++
  // 公共功能模块
  {
    path: '/publicModule/materialManage',
    component: Layout,
    redirect: '/publicModule/materialManage/index',
    // alwaysShow: true,
    meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '材价管理', icon: 'iconfont icon-caijiaguanli' },
    children: [
      {
        path: 'index',
        name: 'materialManage',
        component: () => import('@/views/publicModule/materialManage'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '材价管理', breadcrumb: false, activeMenu: '/publicModule/materialManage/index' }
      },
      {
        path: 'projectDetails',
        name: 'projectDetails',
        component: () => import('@/views/publicModule/materialManage/projectDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '项目详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'infoPriceDetails',
        name: 'infoPriceDetails',
        component: () => import('@/views/publicModule/materialManage/infoPriceDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '信息价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'averagePriceDetails',
        name: 'averagePriceDetails',
        component: () => import('@/views/publicModule/materialManage/averagePriceDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '信息均价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'marketPriceDetails',
        name: 'marketPriceDetails',
        component: () => import('@/views/publicModule/materialManage/marketPriceDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '市场材价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      }
    ]
  },
  {
    path: '/publicModule/setting',
    component: Layout,
    redirect: '/publicModule/setting',
    hidden: true,
    meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', icon: 'iconfont icon-caijiaguanli' },
    children: [
      {
        path: 'materialSet',
        name: 'materialSet',
        component: () => import('@/views/publicModule/setting/materialSet'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '材价设置' }
      }
    ]
  }
  // menuKey 为none，不判断权限
  // 别加 path: '*' ，会影响切换系统，404放在路由守卫中判断
  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', meta: { system: ['main', 'caisuan', 'zixun'], menuKey: 'none' }, hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// 重置路由，在退出或切换系统时调用
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
// 路由配置扁平化为对象，key为子父路由拼接后的path
function toflatRoutes(allRoutes, parentPath = '') {
  let temp = {}
  allRoutes.forEach(route => {
    const path = `${parentPath ? (parentPath === '/' ? '' : parentPath) + '/' : ''}${route.path}`
    temp[path] = {
      name: route.name,
      redirect: route.redirect,
      meta: route.meta
    }
    if (route.children?.length > 0) {
      temp = { ...temp, ...toflatRoutes(route.children, path) }
    }
  })
  return temp
}
// 可以在路由守卫中根据flatAllRoutes判断是否切换系统
export const flatAllRoutes = toflatRoutes([...constantRoutes, ...asyncRoutes])
console.log(flatAllRoutes, 'flatAllRoutes')

export default router
