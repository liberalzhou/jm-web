import request from '@/utils/request'
// 上传信息价库的图片
export function uploadImg(data) {
  return request({
    url: '/mpccollect/messagePrice/uploadImg',
    method: 'post',
    data
  })
}

// 删除信息价库的图片
export function deleteUploadImg(params) {
  return request({
    url: '/mpccollect/messagePrice/deleteUploadImg',
    method: 'post',
    params
  })
}

// 信息价库列表
export function getMessagePriceListPage(data) {
  return request({
    url: '/mpccollect/messagePrice/getMessagePriceListPage',
    method: 'post',
    data
  })
}

// 导出信息价库列表excel
export function exportMessagePriceFile(data) {
  return request({
    url: '/mpccollect/messagePrice/exportMessagePriceFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 全部导出信息价库列表excel
export function exportAllMessagePriceFile(data) {
  return request({
    url: '/mpccollect/messagePrice/exportAllMessagePriceFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 修改信息价库信息(修改/发布/删除)
export function updMsgPrice(data) {
  return request({
    url: '/mpccollect/messagePrice/updMsgPrice',
    method: 'post',
    data
  })
}

// 刪除信息价
export function delMessagePrice(data) {
  return request({
    url: '/mpccollect/messagePrice/delMessagePrice',
    method: 'post',
    data
  })
}

// ---------------------------------------------------------------------

// 根据信息价ID获取发布信息
export function getReleaseRecordListById(params) {
  return request({
    url: '/mpccollect/journal/getReleaseRecordListById',
    method: 'get',
    params
  })
}

// 信息价库详情列表
export function getMessagePriceDetailListPage(data) {
  return request({
    url: '/mpccollect/messagePriceDetail/getMessagePriceDetailListPage',
    method: 'post',
    data
  })
}

// 调整序号
export function operateSortNo(params) {
  return request({
    url: '/mpccollect/messagePriceDetail/operateSortNo',
    method: 'post',
    params
  })
}

// 导出信息价库详情列表excel
export function exportMessagePriceDetailFile(params) {
  return request({
    url: '/mpccollect/messagePriceDetail/exportMessagePriceDetailFile',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 获取材料信息价趋势
export function getMessagePriceListByName(params) {
  return request({
    url: '/mpccollect/messagePriceDetail/getMessagePriceListByName',
    method: 'get',
    params
  })
}

// 发布记录列表
export function getReleaseRecordListPage(data) {
  return request({
    url: '/mpccollect/messagePrice/getReleaseRecordListPage',
    method: 'post',
    data
  })
}

// 回收站列表
export function getRecycleListPage(data) {
  return request({
    url: '/mpccollect/messagePrice/getRecycleListPage',
    method: 'post',
    data
  })
}

// 还原信息价
export function restoreMsgPrice(params) {
  return request({
    url: '/mpccollect/messagePrice/restoreMsgPrice',
    method: 'post',
    params
  })
}

// 彻底删除信息价
export function thoroughDelMsgPrice(params) {
  return request({
    url: '/mpccollect/messagePrice/thoroughDelMsgPrice',
    method: 'post',
    params
  })
}

// 历史信息价库列表
export function getMessagePriceHistoryListPage(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/getMessagePriceHistoryListPage',
    method: 'post',
    data
  })
}

// 删除历史信息价清单
export function batchDelHistoryMsgPrice(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/batchDelHistoryMsgPrice',
    method: 'post',
    params
  })
}

// 历史信息价库详情列表
export function getMessagePriceDetailHistoryListPage(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/getMessagePriceDetailHistoryListPage',
    method: 'post',
    data
  })
}

// 导出历史信息价库详情列表excel
export function exportMessagePriceHistoryFile(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/exportMessagePriceHistoryFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 下载历史信息价库模板excel
export function downloadMsgPriceHistoryFile() {
  return request({
    url: '/mpccollect/messagePriceHistory/downloadMsgPriceHistoryFile',
    method: 'post',
    responseType: 'blob'
  })
}

// 导入历史信息价
export function importHistoryMsgPrice(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/importHistoryMsgPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取发布平台
export function getReleasePlatformList(data) {
  return request({
    url: '/mpccollect/messagePrice/getReleasePlatformList',
    method: 'get'
  })
}

// 修改历史信息价
export function updHistoryMsgPrice(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/updHistoryMsgPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 发布信息价
export function releaseMsgPrice(data) {
  return request({
    url: '/mpccollect/messagePrice/releaseMsgPrice',
    method: 'post',
    data
  })
}

// 根据周期校验信息价是否已存在
export function checkMsgPriceExists(params) {
  return request({
    url: '/mpccollect/messagePrice/checkMsgPriceExists',
    method: 'get',
    params
  })
}

// 期刊库列表
export function getJournalListPage(data) {
  return request({
    url: '/mpccollect/journal/getJournalListPage',
    method: 'post',
    data
  })
}

// 新增期刊库信息
export function addJournal(data) {
  return request({
    url: '/mpccollect/journal/addJournal',
    method: 'post',
    data
  })
}

// 修改期刊库信息
export function updJournal(data) {
  return request({
    url: '/mpccollect/journal/updJournal',
    method: 'post',
    data
  })
}

// 发布期刊
export function releaseJournal(params) {
  return request({
    url: '/mpccollect/journal/releaseJournal',
    method: 'post',
    params
  })
}

// 删除期刊
export function delJournal(data) {
  return request({
    url: '/mpccollect/journal/delJournal',
    method: 'post',
    data
  })
}

// 期刊库详情列表
export function getJournalDetailListPage(data) {
  return request({
    url: '/mpccollect/journal/getJournalDetailListPage',
    method: 'post',
    data
  })
}

// 根据信息价ID获取自定义分类/国标分类
export function getTypeByMsgId(params) {
  return request({
    url: '/mpccollect/journal/getTypeByMsgId',
    method: 'get',
    params
  })
}

// 添加信息价
export function addMsgPrice(data) {
  return request({
    url: '/mpccollect/journal/addMsgPrice',
    method: 'post',
    data
  })
}

// 移除信息价
export function removeMsgPrice(params) {
  return request({
    url: '/mpccollect/journal/removeMsgPrice',
    method: 'post',
    params
  })
}

// 修改期刊详情材料序号
export function updJournalDetailSort(params) {
  return request({
    url: '/mpccollect/journal/updJournalDetailSort',
    method: 'post',
    params
  })
}

// 导出期刊库详情列表excel
export function exportJournalDetailFile(params) {
  return request({
    url: '/mpccollect/journal/exportJournalDetailFile',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 发布记录详情列表
export function getReleaseRecordDetailListPage(data) {
  return request({
    url: '/mpccollect/messagePrice/getReleaseRecordDetailListPage',
    method: 'post',
    data
  })
}

// 根据期刊ID获取信息价名称
export function getMsgNameById(params) {
  return request({
    url: '/mpccollect/journal/getMsgNameById',
    method: 'get',
    params
  })
}

// 获取信息价库或期刊库自定义分类
export function getTempCustomType(params) {
  return request({
    url: '/mpccollect/messagePriceDetail/getCustomTypeList',
    method: 'get',
    params
  })
}

// 历史库在造价通获取造价站地区信息-期刊库列表
export function costStationPrice(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/costStationPrice',
    method: 'post',
    params
  })
}

// 历史库在造价通获取造价站地区信息-详情
export function costStationPriceDetail(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/costStationPriceDetail',
    method: 'post',
    params
  })
}

// 历史信息价详情导入模板下载-标准
export function pipUploadTemp(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/msgPriceHistoryDetailFile',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 历史信息价详情导入模板下载-自定义
export function costUploadTemp(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/cusMsgPriceHistoryDetailFile',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 导入历史信息价详情
export function infoImportFile(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/importHistoryMsgPriceDetail',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 批量添加历史信息价
export function addCostStationPrice(data) {
  return request({
    url: '/mpccollect/messagePriceHistory/addCostStationPrice',
    method: 'post',
    data
  })
}

// 新增单条历史 标准化/自定义 信息价
export function addSingleInfoPrice(data, params) {
  return request({
    url: '/mpccollect/messagePriceHistory/addMaterial',
    method: 'post',
    data,
    params
  })
}

// 删除历史信息价详情数据
export function delInfoPrice(params) {
  return request({
    url: '/mpccollect/messagePriceHistory/batchDelHistoryMsgPriceDetail',
    method: 'post',
    params
  })
}

// 删除历史信息价详情数据
export function editInfoPrice(data, params) {
  return request({
    url: '/mpccollect/messagePriceHistory/editMaterial',
    method: 'post',
    data,
    params
  })
}

