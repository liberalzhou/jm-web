import request from '@/utils/request'

// 获取图形验证码
export function authImg() {
  return request({
    url: '/passport/authImg',
    method: 'get',
    responseType: 'arraybuffer'
  })
}

// 登录
export function login(data) {
  return request({
    url: '/mpcsys/login/loginmain',
    method: 'post',
    data
  })
}

// 退出
export function logout() {
  return request({
    url: '/collect/messager/logout',
    method: 'get'
  })
}

// 查询采集员列表
export function getUsersList(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/getCollectMessagerInfoDetailPage',
    method: 'post',
    data
  })
}

// 获取上报类型列表
export function getQuoteTypeList() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getQuoteTypeList',
    method: 'post'
  })
}

// 删除采集员信息
export function deleteUsers(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/deleteCollectMessagerInfo',
    method: 'post',
    params
  })
}

// 导出采集员信息
export function exportUsers(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/exportCollectMessagerInfoFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 重置密码
export function initPassword(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/resetPwd',
    method: 'post',
    params
  })
}

// 发送邮箱
export function sendEmailMessage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/sendEmailMessage',
    method: 'post',
    data
  })
}

// 发送短信
export function sendSmsMessage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/sendSmsMessage',
    method: 'post',
    data
  })
}

// 获取地区
export function getRegion() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getAddressTree',
    method: 'post'
  })
}

// 创建或更新采集员信息
export function createOrUpdateUser(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/addOrUpdateCollectMessager',
    method: 'post',
    data
  })
}

// 批量开通采集员账号
export function batchUsers(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/batchAddAccount',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 下载批量开通采集员账号模板
export function getAddAccountImportTemplate() {
  return request({
    url: '/mpccollect/collectMessagerInfo/getAddAccountImportTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 查询审核标准信息
export function getAuditInfo() {
  return request({
    url: '/mpccollect/config/getAuditInfo',
    method: 'post'
  })
}

// 查询通知模板列表
export function getTemplateInfoList(data) {
  return request({
    url: '/mpccollect/config/getTemplateInfoList',
    method: 'post',
    data
  })
}

// 查询采集员通知列表
export function getNoticeListPage(data) {
  return request({
    url: '/mpccollect/collectMessagerInfo/getNoticeListPage',
    method: 'post',
    data
  })
}

// 查询邮箱列表
export function getEmailList(data) {
  return request({
    url: '/mpccollect/config/getEmailList',
    method: 'post'
  })
}

// 采集员审核状态修改
export function changeAuditState(params) {
  return request({
    url: '/mpccollect/collectMessagerInfo/auditCollectMessagerInfo',
    method: 'post',
    params
  })
}

// 更新审核标准信息
export function changeAuditText(params) {
  return request({
    url: '/mpccollect/config/updateAudit',
    method: 'post',
    params
  })
}

// 删除通知模板
export function delInformTemplate(params) {
  return request({
    url: '/mpccollect/config/delInformTemplate',
    method: 'post',
    params
  })
}

// 新增/编辑通知模板
export function addOrUpdateInformTemplate(data) {
  return request({
    url: '/mpccollect/config/addOrUpdateInformTemplate',
    method: 'post',
    data
  })
}

// 新增或修改邮箱
export function addOrUpdateEmail(data) {
  return request({
    url: '/mpccollect/config/addOrUpdateEmail',
    method: 'post',
    data
  })
}

// 删除邮箱
export function delEmail(params) {
  return request({
    url: '/mpccollect/config/delEmail',
    method: 'post',
    params
  })
}

// 下载初始化模板
export function downloadInitTemplateFile() {
  return request({
    url: '/mpcsys/sett/downloadInitTemplateFile',
    method: 'post',
    responseType: 'blob'
  })
}

// 导入初始化模板材料
export function importInitTempData(data) {
  return request({
    url: '/mpcsys/sett/importInitTempData',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取采集员上传材价模板列表
export function getUploadMaterialPriceList(data) {
  return request({
    url: '/collect/messager/uploadDetail/selectUploadDetail',
    method: 'post',
    data
  })
}

// 采集员上传材价
// export function collectrImportMatPrice(data) {
//   return request({
//     url: '/mpccollect/collectMessagerInfo/collectrImportMatPrice',
//     method: 'post',
//     data
//   })
// }

// 采集员上传材价
export function collectrImportMatPrice(data) {
  return request({
    url: '/collect/messager/uploadDetail/uploadTempFile',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 采集员材价文件模板下载
export function downloadTempFile(params) {
  return request({
    url: '/collect/messager/uploadDetail/downloadTempFile',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 初始化设置
export function initSet(data) {
  return request({
    url: '/mpcsys/sett/addInitializationSett',
    method: 'post',
    data
  })
}

// 根据ID或创建人获取初始化信息
export function getInitializationSettById(params) {
  return request({
    url: '/mpcsys/sett/getInitializationSettById',
    method: 'get',
    params
  })
}

// 修改初始化信息
export function updInitializationSett(data) {
  return request({
    url: '/mpcsys/sett/updInitializationSett',
    method: 'post',
    data
  })
}

// 删除材料信息
export function delPeriodPrice(data) {
  return request({
    url: '/mpcsys/sett/delPeriodPrice',
    method: 'post',
    data
  })
}

// 获取当前登录用户信息
export function getCurrUser(params) {
  return request({
    url: '/mpcsys/login/getCurrUser',
    method: 'post',
    params
  })
}

// 获取信息价发布设置
export function getReleaseMsgPriceSett() {
  return request({
    url: '/mpccollect/messagePrice/getReleaseMsgPriceSett',
    method: 'post'
  })
}

// 信息价发布批注设置(type为必填(0:表头 1:表尾))
export function releaseNotationSett(params) {
  return request({
    url: '/mpccollect/messagePrice/releaseNotationSett',
    method: 'post',
    params
  })
}

// 信息价发布设置
export function releaseMsgPriceSett(data) {
  return request({
    url: '/mpccollect/messagePrice/releaseMsgPriceSett',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 下载文件
export function downloadFile(params) {
  return request({
    url: '/mpccollect/messagePrice/downloadFile',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 获取主材清单
export function getMainMaterialList() {
  return request({
    url: '/mpccollect/collectTemplateDetail/getMainMaterialList',
    method: 'get'
  })
}

// 样本筛选设置
export function coefficientValueConfig(params) {
  return request({
    url: '/complex/config/coefficientValueConfig',
    method: 'post',
    params
  })
}

// 获取样本筛选设置
export function getCoefficientValueConfig() {
  return request({
    url: '/complex/config/getCoefficientValueConfig',
    method: 'post'
  })
}

// 获取正态分布模型设置
export function getPNModelConfig() {
  return request({
    url: '/complex/config/getPNModelConfig',
    method: 'post'
  })
}

// 算法模型设置
export function calMatModelConfig(params) {
  return request({
    url: '/complex/config/calMatModelConfig',
    method: 'post',
    params
  })
}

// 设置主材
export function setMainMaterial(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/setMainMaterial',
    method: 'post',
    params
  })
}

// 移除主材
export function removeMainMaterial(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/removeMainMaterial',
    method: 'post',
    params
  })
}

// 主材序号 ?
export function sortMainMaterial(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/adjustmentNumber',
    method: 'post',
    params
  })
}

// 账号管理列表
export function selectUser(data) {
  return request({
    url: '/sz2sys/userExtend/selectUser',
    method: 'post',
    data
  })
}

// 删除账号
export function deleteUser(params) {
  return request({
    url: '/sz2sys/userExtend/deleteUser',
    method: 'post',
    params
  })
}

// 重置密码
export function resetPwd(params) {
  return request({
    url: '/sz2sys/userExtend/resetPwd',
    method: 'post',
    params
  })
}

// 获取角色名称列表
export function getRoleList() {
  return request({
    url: '/sz2sys/userExtend/getRoleList',
    method: 'post'
  })
}

// 新增
export function addUser(params) {
  return request({
    url: '/sz2sys/userExtend/addUser',
    method: 'post',
    params
  })
}

// 更新账号
export function updateUser(params) {
  return request({
    url: '/sz2sys/userExtend/updateUser',
    method: 'post',
    params
  })
}

// 更换角色
export function updateRole(params) {
  return request({
    url: '/sz2sys/userExtend/updateRole',
    method: 'post',
    params
  })
}

// 角色列表
export function selectRole(data) {
  return request({
    url: '/sz2sys/permission/selectRole',
    method: 'post',
    data
  })
}

// 批量删除角色
export function deleteRole(params) {
  return request({
    url: '/sz2sys/permission/deleteRole',
    method: 'post',
    params
  })
}

// 获取权限列表数据
export function getListDate(params) {
  return request({
    url: '/sz2sys/permission/getListDate',
    method: 'post',
    params
  })
}

// 获取所有的权限id
export function allMenu() {
  return request({
    url: '/sz2sys/permission/allMenu',
    method: 'post'
  })
}

// 新增角色
export function addPermission(params) {
  return request({
    url: '/sz2sys/permission/addPermission',
    method: 'post',
    params
  })
}

// 编辑角色
export function editPermission(params) {
  return request({
    url: '/sz2sys/permission/editPermission',
    method: 'post',
    params
  })
}

// 模板设置
export function setTemplate(data) {
  return request({
    url: '/mpccollect/collectTemplate/setTemplate',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取模板设置
export function getTemplateSett() {
  return request({
    url: '/mpccollect/collectTemplate/getTemplateSett',
    method: 'get'
  })
}

// 综合价设置
export function addOrUpdateSysDict(data) {
  return request({
    url: '/complex/config/addOrUpdateSysDict',
    method: 'post',
    data
  })
}

// 自定测算设置
export function autoPriceCalConfig(data) {
  return request({
    url: '/complex/config/autoPriceCalConfig',
    method: 'post',
    data
  })
}

// 上传用户头像
export function uploadUserImg(data) {
  return request({
    url: '/sz2sys/userExtend/uploadUserImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 更新账号密码
export function updatePwd(params) {
  return request({
    url: '/sz2sys/userExtend/updatePwd',
    method: 'post',
    params
  })
}

// 根据时间获取周期
export function getPeriodByDate(params) {
  return request({
    url: '/mpcsys/sett/getPeriodByDate',
    method: 'get',
    params
  })
}

// 获取发布平台
export function getPlatformList() {
  return request({
    url: '/mpccollect/messagePrice/getPlatformList',
    method: 'get'
  })
}

// 新增发布平台
export function addPlatform(data) {
  return request({
    url: '/mpccollect/messagePrice/addPlatform',
    method: 'post',
    data
  })
}

// 编辑发布平台
export function updatePlatform(data) {
  return request({
    url: '/mpccollect/messagePrice/updatePlatform',
    method: 'post',
    data
  })
}

// 删除发布平台
export function deletePlatform(params) {
  return request({
    url: '/mpccollect/messagePrice/deletePlatform',
    method: 'post',
    params
  })
}
