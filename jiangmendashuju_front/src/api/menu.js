import request from '@/utils/request'

// 查询菜单树状列表(公共部分)
export function getCommonMenuTree() {
  return request({
    url: '/sys/getCommonMenuTree',
    method: 'POST'
  })
}

// 查询菜单树状列表(咨询公司)
export function getMenuTree() {
  return request({
    url: '/sys/getMenuTree',
    method: 'POST'
  })
}

// 查询菜单树状列表(采算编发)
export function getMpcMenuTree() {
  return request({
    url: '/sys/getMpcMenuTree',
    method: 'POST'
  })
}
