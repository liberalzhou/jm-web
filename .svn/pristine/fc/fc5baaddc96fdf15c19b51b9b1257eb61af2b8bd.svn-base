<template>
  <div class="examine-container">
    <!-- 顶部筛选 -->
    <div class="screen">
      <div class="flex-c top">
        <!-- 搜索 -->
        <div class="com-search2">
          <el-input v-model="searchVal" class="search-ipt" placeholder="搜索关键字，如:项目、批次" @keyup.enter.native="search" />
          <i class="el-icon-search" @click="search" />
        </div>
        <!-- 下拉 -->
        <div class="flex-c com-select cycle-wrap">
          <span class="com-label-text">审核状态：</span>
          <el-select v-model="curStatus" class="com-radius-input" placeholder="请选择" @change="search">
            <el-option v-for="item in statusList" :key="item.value" :label="item.name" :value="item.value" />
          </el-select>
        </div>
        <!-- 时间 -->
        <div class="search-type">
          <div class="flex-c">
            <span class="com-label-text" style="font-weight: 400; min-width: 60px; white-space: nowrap">提交时间：</span>
            <div class="com-time-search">
              <el-date-picker v-model="startDate" type="date" value-format="yyyy-MM-dd" :picker-options="pickerOptions1" placeholder="选择日期" @change="search" />
            </div>
            <span class="com-line" />
            <div class="com-time-search">
              <el-date-picker v-model="endDate" type="date" value-format="yyyy-MM-dd" placeholder="选择日期" :picker-options="pickerOptions2" @change="search" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- 表内容 -->
    <div class="com-table table-wrap inquiry-table">
      <div class="table-box">
        <el-table v-loading="loading" :data="tableData" tooltip-effect="dark" class="table-main" style="width: 100%" @selection-change="multipleSelection = $event">
          <el-table-column width="240px" label="批次名称">
            <template slot-scope="scope">
              <div v-has.show="'inquire:manage:selectInquiry'" class="flex-cc mode-name" @click="skipDetail(scope.row)">
                <span>{{ scope.row.piciName }}</span>
                <img v-if="scope.row.isUrgent == 1" src="@/assets/img/icons/faster.png" alt="" class="faster-img">
              </div>
            </template>
          </el-table-column>
          <el-table-column width="220px" label="项目名称">
            <template slot-scope="scope">
              <span class="t-color wrap">{{ scope.row.proName }}</span>
            </template>
          </el-table-column>
          <!-- 询价类型(0:清单询价 1:附件询价) -->
          <el-table-column label="询价类型">
            <template slot-scope="scope">
              <span v-if="scope.row.inquiryType == 0" class="t-color">清单询价</span>
              <span v-else class="t-color">附件询价</span>
            </template>
          </el-table-column>
          <el-table-column prop="materialCount" label="材料数量" />
          <el-table-column prop="needCount" label="报价数" />
          <el-table-column prop="inquiryAccount" label="询价账号" />
          <el-table-column prop="submitBy" label="提交人" />
          <el-table-column width="90px" label="提交时间">
            <template slot-scope="scope">
              <span>{{ formatDate(scope.row.submitTime, "1") }}</span>
            </template>
          </el-table-column>
          <!-- 审核状态(1:待审核 2:审核通过 3:审核不通过) -->
          <el-table-column label="审核状态">
            <template slot-scope="scope">
              <span v-if="scope.row.auditStatus == 1">待审核</span>
              <span v-if="scope.row.auditStatus == 2" class="pass">审核通过</span>
              <span v-if="scope.row.auditStatus == 3" class="no">审核不通过</span>
            </template>
          </el-table-column>
          <!-- <el-table-column label="审核人"> -- </el-table-column>
          <el-table-column label="审核时间">
            --
          </el-table-column>  -->
          <el-table-column prop="reviewer" label="审核人" />
          <el-table-column width="90px" prop="createTime" label="审核时间">
            <template slot-scope="scope">
              <span>{{ formatDate(scope.row.createTime, "1") }}</span>
            </template>
          </el-table-column>
          <el-table-column label="操作" width="250px">
            <template slot-scope="scope">
              <div class="flex-c">
                <div v-if="scope.row.auditStatus == 1 && $_has('inquiryAudit:auditInquiry')" class="flex-c examine">
                  <div class="flex-c yes" @click="showExamine(scope.row.id, 2)">
                    <img src="@/assets/img/icons/is_pass.png" alt="">审核通过
                  </div>
                  <div class="flex-c no" @click="showExamine(scope.row.id, 3)">
                    <img src="@/assets/img/icons/no_pass.png" alt="">审核不通过
                  </div>
                </div>
                <div class="icon-wrap">
                  <span v-if="scope.row.auditStatus == 3 && scope.row.inquiryType == 0" class="t-color" @click="skipDetail(scope.row, 'details')">
                    <el-tooltip effect="light" content="编辑询价" placement="top">
                      <img src="@/assets/img/icons/edits.png" alt="">
                    </el-tooltip>
                  </span>
                  <span>
                    <el-tooltip effect="light" content="审核记录" placement="top">
                      <img src="@/assets/img/icons/examine_icon.png" alt="" @click="showAuditRecord(scope.row.id)">
                    </el-tooltip>
                  </span>
                </div>
              </div>
            </template>
          </el-table-column>
        </el-table>
        <!-- 分页 -->
        <div class="flex-csb com-pages-wrap">
          <div class="flex-c left-page">
            <span>当前显示</span>
            <div class="flex-cc cur-page">
              <el-select v-model="pageSize" placeholder="请选择" @change="handleCurrentChange(1)">
                <el-option v-for="(item, index) in pageSizeList" :key="index" :label="item" :value="item" />
              </el-select>
            </div>
            <span>条</span>
            <span class="total-num">共{{ totalCount }}条数据</span>
          </div>
          <div class="flex-c right-page">
            <el-pagination background :current-page.sync="currentPage" :page-size="pageSize" layout="prev, pager, next, jumper" :total="totalCount" @current-change="handleCurrentChange" />
            <div class="com-page-btn">确定</div>
          </div>
        </div>
      </div>
    </div>

    <!-- 审核记录 -->
    <el-dialog title="审核记录" class="com-dialog my-dialog" width="600px" :visible.sync="dialogRecordVisible">
      <el-table :data="auditRecordData" :row-class-name="getRowClassName">
        <el-table-column property="status" label="流程操作" width="180">
          <template slot-scope="scope">
            <img v-if="scope.row.status == 2" class="imgs" src="@/assets/img/examine_yes2.png" alt="">
            <img v-if="scope.row.status == 3" class="imgs" src="@/assets/img/examine_no2.png" alt="">
            <img v-if="scope.row.status == 1" class="imgs" src="@/assets/img/examine_pend2.png" alt="">
          </template>
        </el-table-column>
        <el-table-column label="操作时间">
          <template slot-scope="scope">
            <span>{{ formatDate(scope.row.createTime, "1") }}</span>
          </template>
        </el-table-column>
        <el-table-column property="operation" label="操作员" />
        <el-table-column type="expand">
          <template slot-scope="scope">
            <div style="120px">
              <p style="color:#999;text-align:left;">审核批注: </p>
              <p style="text-align:center;">{{ scope.row.remark }}</p>
            </div>
          </template>
        </el-table-column>
      </el-table>
    </el-dialog>

    <!-- 审核弹窗 -->
    <el-dialog title="询价审核" :visible.sync="showToexamineDialog" width="460px" class="com-dialog my-dialog" :close-on-click-modal="false" @close="remark = ''">
      <img v-if="examineStatus == 1" src="@/assets/img/dialog/confirm.png" alt="">
      <p v-if="examineStatus == 1">确定审核通过？</p>
      <p v-if="examineStatus == 2" style="margin-bottom: 20px; text-align: left; padding-left: 4px">
        确定审核不通过？
      </p>
      <el-input v-if="examineStatus == 2" v-model="remark" type="textarea" resize="none" rows="7" placeholder="请输入审核批注（选填）" maxlength="300" />
      <span slot="footer" class="dialog-footer">
        <el-button @click="showToexamineDialog = false">取消</el-button>
        <el-button type="primary" :loading="toExamineloading" @click="auditInquiry">确定</el-button>
      </span>
    </el-dialog>
  </div>
</template>

<script>
import {
  getInquiryAuditList,
  getAuditRecordListById,
  auditInquiry
} from '@/api/inquiry'
import { formatDate } from '@/utils/common'
export default {
  name: 'InquiryExamine',
  data() {
    return {
      formatDate: formatDate,
      searchVal: '', // 搜索关键字
      curStatus: null, // 当前选中的审核状态
      statusList: [
        {
          value: null,
          name: '全部'
        },
        {
          value: 1,
          name: '待审核'
        },
        {
          value: 2,
          name: '审核通过'
        },
        {
          value: 3,
          name: '审核不通过'
        }
      ], // 审核状态列表
      startDate: null,
      endDate: null,
      tableData: [], // 表格数据
      loading: false,
      currentPage: 1,
      pageSize: 10,
      totalCount: 0,
      pageSizeList: [10, 20, 30, 40, 50, 100],
      dialogRecordVisible: false, // 是否显示审核记录
      auditRecordData: [], // 审核记录
      showToexamineDialog: false, // 显示审核二次确认弹窗
      toExamineloading: false, // 二次确认loading
      isInquiryId: '', // 当前选中批次ID
      isAuditStatus: null, // 当前审核状态
      examineStatus: '', // 审核弹窗状态 1通过 2不通过
      remark: '', // 审核不通过理由
      pickerOptions1: {
        // 开始时间不可大于结束时间
        disabledDate: time => {
          if (this.endDate) {
            const end = this.endDate + ' 23:59:59' // 日期详细才能选中当天
            return time.getTime() > new Date(end)
          }
        }
      },
      pickerOptions2: {
        // 结束时间不可小于开始时间
        disabledDate: time => {
          if (this.startDate) {
            const start = this.startDate + ' 00:00:00'
            return time.getTime() < new Date(start)
          }
        }
      }
    }
  },
  mounted() {
    this.getInquiryAuditList()
  },
  methods: {
    // 搜索
    search() {
      this.currentPage = 1
      this.getInquiryAuditList()
    },
    // 选择审核状态
    changeStatus() {},
    // 选择开始时间
    toggleDate() {
      this.getInquiryAuditList()
    },
    // 选择结束时间
    toggleDate2() {
      this.getInquiryAuditList()
    },
    // 切换分页
    handleCurrentChange(val) {
      this.currentPage = val
      // this.getDataList()
      this.getInquiryAuditList()
    },
    // 去批次详情
    skipDetail(item, type) {
      if (type == 'details') {
        this.$router.push({
          path: '/publicModule/inquiryManage/editInquiry',
          query: {
            piciId: item.id,
            type: item.inquiryType,
            decide: 0
          }
        })
      } else if (item.auditStatus == 1) {
        // 待审核
        this.$router.push({
          path: '/publicModule/inquiryManage/examinePend',
          query: {
            status: 1,
            id: item.id,
            piciName: item.piciName,
            proName: item.proName,
            inquiryType: item.inquiryType
          }
        })
      } else if (item.auditStatus == 2) {
        // 审核通过
        this.$router.push({
          path: '/publicModule/inquiryManage/examinePass',
          query: {
            status: 2,
            id: item.id,
            piciName: item.piciName,
            proName: item.proName,
            inquiryType: item.inquiryType
          }
        })
      } else if (item.auditStatus == 3) {
        // 审核不通过
        this.$router.push({
          path: '/publicModule/inquiryManage/examineFail',
          query: {
            status: 3,
            id: item.id,
            piciName: item.piciName,
            proName: item.proName,
            inquiryType: item.inquiryType
          }
        })
      }
    },
    // 获取询价审核列表
    getInquiryAuditList() {
      const data = {
        currentPage: this.currentPage,
        pageSize: this.pageSize,
        keyword: this.searchVal ? this.searchVal : null,
        startDate: this.startDate ? this.startDate + ' 00:00:00' : '',
        endDate: this.endDate ? this.endDate + ' 23:59:59' : ''
      }

      if (this.curStatus) data.auditStatus = this.curStatus
      this.tableLoading = true
      getInquiryAuditList(data)
        .then(res => {
          console.log(res)
          if (res.code == '01') {
            this.tableData = res.body.list
            this.totalCount = res.body.totalCount
          } else {
            this.$message({
              type: 'error',
              message: res.message || '出错了'
            })
          }
        })
        .catch(err => {
          this.loading = false
          this.$message({
            type: 'error',
            message: err.message || '出错了'
          })
        })
    },
    // 控制展开审核批注样式
    getRowClassName({ row, rowIndex }) {
      if (!row.remark) {
        return 'row-expand-cover'
      }
    },
    // 显示审核记录
    showAuditRecord(id) {
      getAuditRecordListById({ id })
        .then(res => {
          console.log('审核记录', res)
          if (res.code == '01') {
            this.auditRecordData = res.body
            this.dialogRecordVisible = true
          } else {
            this.$message({
              type: 'error',
              message: res.message || '出错了'
            })
          }
        })
        .catch(err => {
          this.$message({
            type: 'error',
            message: err.message || '出错了'
          })
        })
    },
    // 显示审核二次确认弹窗
    showExamine(id, type) {
      this.isInquiryId = id
      this.isAuditStatus = type
      this.showToexamineDialog = true

      if (type == 3) {
        this.examineStatus = 2
      } else {
        this.examineStatus = 1
      }
    },
    // 审核操作
    auditInquiry() {
      const obj = {
        inquiryId: this.isInquiryId + '',
        auditStatus: this.isAuditStatus
      }
      if (this.isAuditStatus == 3) {
        // 审核不通过
        obj.remark = this.remark
      }
      this.toExamineloading = true
      // 审核通过
      auditInquiry(obj)
        .then(res => {
          if (res.code == '01') {
            this.$message({
              type: 'success',
              message: res.message || '审核成功！'
            })
            this.getInquiryAuditList()
          } else {
            this.$message({
              type: 'error',
              message: res.message || '出错了'
            })
          }

          this.showToexamineDialog = false
          this.toExamineloading = false
        })
        .catch(err => {
          this.$message({
            type: 'error',
            message: err.message || '出错了'
          })
          this.showToexamineDialog = false
          this.toExamineloading = false
        })
    }
  }
}
</script>

<style lang="scss" scoped>
@import '../../styles/variables.scss';
.examine-container {
  // 筛选
  .screen {
    border-radius: 0 0 6px 6px;

    .top {
      flex-wrap: wrap;
    }
  }
  .inquiry-table {
    margin-top: 18px;
    border-radius: 6px;
    overflow: hidden;
    .inquiry {
      background: #fcfcfd;
      .btn {
        width: 112px;
        height: 34px;
        color: #ffffff !important;
        border-radius: 5px;
        font-weight: 400;
        .el-icon-search {
          margin-right: 5px;
        }
      }
    }
    .table-box {
      background: #ffffff;
      padding-bottom: 60px;
      .examine {
        font-size: 13px;
        height: 16px;
        font-family: 'mainfont';
        margin-right: 17px;
        padding-right: 17px;
        border-right: 1px solid #9eb8d0;
        > div {
          width: 80px;
          margin-right: 5px;
          cursor: pointer;
        }
      }
      .yes {
        color: $color-primary;
      }
      .no {
        color: $color-danger;
      }
      .pass {
        color: #1eb645;
      }
      .t-color {
        font-weight: 400;
        color: $table-color;
      }
      .faster-img {
        width: 44px;
        height: 19px;
        margin-left: 5px;
      }
      .wrap {
        width: 214px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
      }
      .mode-name {
        cursor: pointer;
        color: $color-primary;
      }
    }
  }
}
</style>

<style lang="scss">
.examine-container {
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background: #f6f6f6;
  .screen {
    padding: 0 21px 5px;
    background: #ffffff;
    .cycle-wrap {
      padding: 20px;
      > span {
        margin-right: 10px;
        min-width: 60px;
        font-weight: 400;
        white-space: nowrap;
      }
      .el-select {
        input {
          width: 281px;
          height: 36px;
        }
      }
    }
  }
  .table-box {
    .has-gutter {
      font-size: 14px;
    }
    .el-table {
      color: #5f5f5f;
      font-size: 13px;
      font-weight: 400;
    }
  }

  .imgs {
    width: 58px;
  }
}
.row-expand-cover .el-table__expand-icon {
  visibility: hidden;
}
</style>
