import { asyncRoutes, constantRoutes } from '@/router'

// 判断路由权限
function hasPermission(menuList, route) {
  if (route.meta && route.meta.menuKey) {
    return menuList.findIndex(item => item.menuKey === route.meta.menuKey) !== -1 || route.meta.menuKey === 'none'
  }
}
// 递归遍历过滤异步路由表
function filterAsyncRoutes(asyncRoutes, menuList) {
  return asyncRoutes.filter(route => {
    const temp = { ...route }
    if (hasPermission(menuList, temp)) {
      if (temp.children) {
        temp.children = filterAsyncRoutes(temp.children, menuList)
      }
      return true
    }
  })
}

const state = {
  userCommonMenuList: [], // 主系统
  userMpcMenuList: [], // 采算编发
  userMenuList: [], // 咨询
  commonFunctionList: [], // 公共
  routes: [],
  addRoutes: [],
  btnPermission: []
}

const mutations = {
  SET_USER_COMMON_MENU_LIST: (state, menuList) => {
    state.userCommonMenuList = menuList
  },
  SET_USER_MPC_MENU_LIST: (state, menuList) => {
    state.userMpcMenuList = menuList
  },
  SET_USER_MENU_LIST: (state, menuList) => {
    state.userMenuList = menuList
  },
  SET_USER_FUNCTION_LIST: (state, functionList) => {
    state.commonFunctionList = functionList
  },
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  // 创建路由配置和按钮权限表
  createPermission({ state, commit }, { system, menuList }) {
    return new Promise(resolve => {
      // 路由配置
      // let accessedRoutes = filterAsyncRoutes(asyncRoutes, menuList) // 判断权限过滤路由
      // console.log(asyncRoutes.filter(route => !route.meta?.system))
      const accessedRoutes = asyncRoutes.filter(route => route.meta?.system?.includes(system)) // 跳过判断权限（临时需要）

      commit('SET_ROUTES', accessedRoutes)

      // 按钮权限
      // ...

      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
