import caiRequest from '@/apiList/CaiRequest'
import zixRequest from '@/apiList/ZixRequest'

export function loginCaiSuan(data) {
  return caiRequest({
    url: '/mpcsys/login/loginmain',
    method: 'post',
    data
  })
}

export function loginZiXun(params) {
  return zixRequest({
    url: '/sys/login/login',
    method: 'post',
    params
  })
}
