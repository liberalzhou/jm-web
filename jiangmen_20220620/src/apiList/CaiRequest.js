import axios from 'axios'
import router from '@/router'
import store from '@/store'
import { Message } from 'element-ui'

const service = axios.create({
  // baseURL: config.baseUrl,
  baseURL: '/mpc-api',
  timeout: 6000
})
// 登录失效的提示弹窗仅显示一条标记
var num = 1

service.interceptors.request.use(
  config => {
      console.log('拦截333333333', config, store.getters.token);
    // config.headers['token'] = store.getters.token
    // 重新登录后重置num次数
    if (config.url.indexOf('/mpcsys/login/loginmain') > -1) {
      num = 1
    }
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    var res = response.data
    console.log('拦截2222', res);
    // 拦截登录失效
    if (res.code == '6008') {
      // 登录失效提示只显示一条
      if (num === 1) {
        Message({
          type: 'warning',
          message: res.message
        })
        num --
      }
      return
    }

    // blob接口未响应流文件，前端终止excel文件生成
    if (response.request.responseType == 'blob') {
      if (response.data.size === 0) {
        Message({
          type: 'error',
          message: '操作失败，请稍后重试'
        })
        return
      }
    }
    // 下载模板时，在响应头获取文件名
    if (response.headers['content-disposition']) {
      // 文件名
      var filename = null
      try {
        var fileStr = decodeURIComponent(response.headers['content-disposition'])
        filename = fileStr.split('filename=')[1]
        // 因为res就为整个文件流，故使用缓存将名字携带至接口调用处
        sessionStorage.setItem('blob_filename', filename)
      } catch(err) {
        console.log('file name err')
      }
    }

    return Promise.resolve(res)
  },
  error => {
    var err = error.response
    console.log(err);
    // Message({
    //   type: 'error',
    //   message: '服务器异常，请稍后再试'
    // })

    return Promise.reject(err.data)
  }
)

export default service
