import { getCurUser, getSysInfo } from '@/api/user'
import { flatAllRoutes, resetRouter } from '@/router'
import { clearStorage } from '@/utils/clearStorage'
import getPageTitle from '@/utils/get-page-title'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import router from './router'
import store from './store'

NProgress.configure({ showSpinner: false }) // 进度条

// const whiteList = ['/login', '/project/importProject', '/materialManage/index'] // 不重定向白名单
const whiteList = ['/login'] // 不重定向白名单

let currentSystem = '' // 避免切换系统时死循环
router.beforeEach(async(to, from, next) => {
  NProgress.start()
  document.title = getPageTitle(to.meta.title)
  // console.log(to, 'to')
  // console.log(from, 'from')
  // console.log('路由变化')
  if (!flatAllRoutes[to.path]) {
    next('/404')
  }
  if (store.getters.token) { // 是否登录(改架构,由请求中Cookie改为token鉴权)
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      if (store.getters.userId) { // 是否已有用户信息
        // 是否切换系统
        const toSystem = flatAllRoutes[to.path]?.meta?.system?.[0]
        if (isSwitchSystem(to, from) && toSystem && currentSystem !== toSystem) {
          currentSystem = toSystem
          switch (currentSystem) {
            case 'main':
              generateAsyncRoutes(to, next, { system: 'main', menuList: store.getters.mainMenuList })
              break
            case 'caisuan':
              generateAsyncRoutes(to, next, { system: 'caisuan', menuList: store.getters.caisuanMenuList })
              break
            case 'zixun':
              generateAsyncRoutes(to, next, { system: 'zixun', menuList: store.getters.zixunMenuList })
              break
          }
        } else {
          next()
        }
      } else {
        getCurUserApi(to, next)
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) { // 白名单
      let token = to.query.token

      if (!token) {
        const str = window.location.search.substring(1)
        const arr = str.split('token=')
        token = arr[1]
      }

      if (token) { // 针对海迈跳转免登录
        store.commit('user/SET_TOKEN', token)
        sessionStorage.setItem('token', token)
        getSysInfoApi()
        getCurUserApi(to, next)
      } else {
        next()
      }
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

// 获取用户信息
async function getCurUserApi(to, next) {
  try {
    const res = await getCurUser()
    if (res.code === '01') {
      sessionStorage.setItem('user_account', res.body.userAccount)
      sessionStorage.setItem('user_name', res.body.userName)
      // sessionStorage.setItem('id', res.body.userId) // 去掉缓存，刷新页面可重置路由表
      sessionStorage.setItem('phone', res.body.phone)
      sessionStorage.setItem('user_head', res.body.avatarUrl)
      sessionStorage.setItem('user_department', res.body.department)
      sessionStorage.setItem('user_position', res.body.position)
      sessionStorage.setItem('user_qq', res.body.qq)
      store.commit('user/SET_USER_ACCOUNT', res.body.userAccount)
      store.commit('user/SET_NAME', res.body.userName)
      store.commit('user/SET_ID', res.body.userId)
      store.commit('user/SET_PHONE', res.body.phone)
      store.commit('user/SET_USER_HEAD', res.body.avatarUrl)
      store.commit('user/SET_USER_DEPARTMENT', res.body.department)
      store.commit('user/SET_USER_POSITION', res.body.position)
      store.commit('user/SET_USER_QQ', res.body.qq)
      // 存储权限表
      store.commit('permission/SET_USER_COMMON_MENU_LIST', res.body.userCommonMenuList || [])
      store.commit('permission/SET_USER_MPC_MENU_LIST', res.body.userMpcMenuList || [])
      store.commit('permission/SET_USER_MENU_LIST', res.body.userMenuList || [])
      store.commit('permission/SET_USER_FUNCTION_LIST', res.body.commonFunctionList || [])
    }
    const toSystem = flatAllRoutes[to.path]?.meta?.system?.[0]
    if (toSystem && currentSystem !== toSystem) {
      currentSystem = toSystem
      switch (currentSystem) {
        case 'caisuan':
          generateAsyncRoutes(to, next, { system: 'caisuan', menuList: store.getters.caisuanMenuList })
          break
        case 'zixun':
          generateAsyncRoutes(to, next, { system: 'zixun', menuList: store.getters.zixunMenuList })
          break
        default:
          generateAsyncRoutes(to, next, { system: 'main', menuList: store.getters.mainMenuList })
      }
    } else {
      next()
    }

    return res
  } catch (error) {
    clearStorage()
    next('/login')
    NProgress.done()
  }
}

// 查询系统信息
function getSysInfoApi() {
  getSysInfo().then(res => {
    if (res.code == '01') {
      try {
        sessionStorage.setItem('belong_company', res.body.belongCompany)
        store.commit('user/SET_BELONG_COMPANY', res.body.belongCompany)
        sessionStorage.setItem('sys_name', res.body.sysName)
        store.commit('user/SET_SYS_NAME', res.body.sysName)
        sessionStorage.setItem('sys_uuid', res.body.sysUuid)
        store.commit('user/SET_SYS_UUID', res.body.sysUuid)
        sessionStorage.setItem('version', res.body.version)
        store.commit('user/SET_VERSION', res.body.version)
        sessionStorage.setItem('technical_support', res.body.technicalSupport)
        store.commit('user/SET_TECHNICAL_SUPPORT', res.body.technicalSupport)
        sessionStorage.setItem('update_time', res.body.updateTime)
        store.commit('user/SET_UPDATE_TIME', res.body.updateTime)
        sessionStorage.setItem('sysLogo', res.body.sysLogo) // fj20220526
        store.commit('user/SET_SYS_LOGO', res.body.sysLogo) // fj20220526
        sessionStorage.setItem('sysId', res.body.id) // fj20220526
        store.commit('user/SET_SYS_ID', res.body.id) // fj20220526
      } catch (err) {
        console.log(err)
      }
    }
  })
}
// 是否切换系统
function isSwitchSystem(to, from) {
  // console.log(to.meta.system, from.meta.system)
  if (Array.isArray(to.meta?.system) && Array.isArray(from.meta?.system)) {
    return !to.meta.system.some(item => {
      return from.meta.system.includes(item)
    })
  } else if (Array.isArray(from.meta.system)) {
    const asyncTo = flatAllRoutes[to.path]
    // console.log('asyncTo=' + asyncTo.meta?.system)
    if (Array.isArray(asyncTo?.meta?.system)) {
      return !asyncTo.meta.system.some(item => {
        return from.meta.system.includes(item)
      })
    }
  }
}

function generateAsyncRoutes(to, next, { system, menuList }) {
  // console.log('generateAsyncRoutes', to, system)
  try {
    store.dispatch('permission/createPermission', { system, menuList }).then(asyncRoutes => {
      // console.log(asyncRoutes)
      resetRouter() // 重置路由
      router.addRoutes(asyncRoutes)
      const query = to.query
      if (query.token) {
        next({ path: to.path, replace: true, query: {
          types: 4,
          id: query.id
        }})
      } else {
        next({ ...to })
      }
    })
  } catch (err) {
    console.log(err)
  }
}
router.afterEach(() => {
  NProgress.done()
})
