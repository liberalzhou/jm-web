// 防抖
export function debounce(fun, time) {
  let timer
  return function() {
    clearTimeout(timer)
    const args = arguments
    timer = setTimeout(() => {
      fun.apply(this, args)
    }, time)
  }
}

// 节流
export function throttle(fun, time) {
  let t1 = 0 // 初始时间
  return function() {
    const t2 = new Date() // 当前时间
    if (t2 - t1 > time) {
      fun.apply(this, arguments)
      t1 = t2
    }
  }
}
