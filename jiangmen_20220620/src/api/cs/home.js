import request from '@/utils/request'

// 获取首页数据
export function getHomePage(params) {
  return request({
    url: '/mpcsys/sett/getHomePage',
    method: 'get',
    params
  })
}
// 获取品牌/供应商数量
export function getBrandAndSpplierAmount(params) {
  return request({
    url: '/mpcsys/sett/getBrandAndSpplierAmount',
    method: 'get',
    params
  })
}

// 获取全国城市信息价数据
export function getNationwideCityPrice(params) {
  return request({
    url: '/mpcsys/sett/getNationwideCityPrice',
    method: 'get',
    params
  })
}
