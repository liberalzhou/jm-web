import request from '@/utils/request'

// 发送短信通知
export function sendSmsMessage(data) {
  return request({
    url: '/sys/sendSmsMessage',
    method: 'POST',
    data
  })
}

// 发送邮件通知
export function sendEmailMessage(data) {
  return request({
    url: '/sys/sendEmailMessage',
    method: 'POST',
    data
  })
}

// 获取账号消息列表
export function getMessagePageListByUserId(params) {
  return request({
    url: '/sys/getMessagePageListByUserId',
    method: 'POST',
    params
  })
}

